/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.ratings;

import eapli.framework.actions.Action;

/**
 *
 * @author Miguel Minnemann (1150457) & Daniel Ribeiro (1150577)
 */
public class RateMealAction implements Action{

    @Override
    public boolean execute() {
        return new RateMealUI().show();
    }
}
