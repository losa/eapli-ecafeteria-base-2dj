/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.nutritionalInfo;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Rui Peixoto
 */
public class AllergensNamePrinter implements Visitor<Allergen>{

    @Override
    public void visit(Allergen visitee) {
        System.out.printf("%-30s\n", visitee.name());
    }
    
}
