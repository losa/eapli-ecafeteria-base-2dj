/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation;

import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.cafeteria.consoleapp.presentation.MyUserMenu;
import eapli.ecafeteria.application.CafeteriaUserBaseController;
import eapli.ecafeteria.domain.cafeteria.BalanceWatchDog;
import eapli.ecafeteria.user.consoleapp.booking.CancelExistingReservationsAction;
import eapli.ecafeteria.user.consoleapp.booking.MakeReservationRequestAction;
import eapli.ecafeteria.user.consoleapp.presentation.meals.ListWeeklyMenuAction;
import eapli.ecafeteria.user.consoleapp.presentation.nutritionalInfo.ChangeNutritionalProfileAction;
import eapli.ecafeteria.user.consoleapp.presentation.nutritionalInfo.ChangeUserAllergensAction;
import eapli.ecafeteria.user.consoleapp.presentation.nutritionalInfo.CheckNutritionalProfileService;
import eapli.ecafeteria.user.consoleapp.presentation.ratings.RateMealAction;
import eapli.framework.actions.ReturnAction;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.ShowMessageAction;
import eapli.framework.presentation.console.ShowVerticalSubMenuAction;
import eapli.framework.presentation.console.SubMenu;
import eapli.framework.presentation.console.VerticalMenuRenderer;
import eapli.framework.presentation.console.VerticalSeparator;
import java.util.Observable;
import java.util.Observer;

/**
 * @author Paulo Gandra Sousa
 */
class MainMenu extends CafeteriaUserBaseUI implements Observer {

    private static final int EXIT_OPTION = 0;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int BOOKINGS_OPTION = 2;
    private static final int ACCOUNT_OPTION = 3;
    private static final int RATINGS_OPTIONS = 4;

    // BOOKINGS MENU
    private static final int LIST_MENUS_OPTION = 1;
    private static final int BOOK_A_MEAL_OPTION = 2;
    private static final int CANCEL_BOOKING_OPTION = 3;
    private static final int LIST_THIS_WEEK_MENUS_OPTION = 4;

    // RATINGS MENU
    private static final int RATE_MEAL_OPTION = 1;

    // ACCOUNT MENU
    private static final int LIST_MOVEMENTS_OPTION = 1;
    private static final int CHANGE_NUTRITIONAL_INFO = 2;
    private static final int CHANGE_USER_ALLERGENS = 3;

    @Override
    public boolean show() {
        createBalanceWatchDog();
        drawFormTitle();
        if (new CheckNutritionalProfileService().getNutritionalProfile() == null) {
            new ChangeNutritionalProfileAction().execute();
        }
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer = new VerticalMenuRenderer(menu);
        return renderer.show();
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
        mainMenu.add(new SubMenu(MY_USER_OPTION, myUserMenu, new ShowVerticalSubMenuAction(myUserMenu)));

        mainMenu.add(VerticalSeparator.separator());

        final Menu bookingsMenu = buildBookingsMenu();
        mainMenu.add(new SubMenu(BOOKINGS_OPTION, bookingsMenu, new ShowVerticalSubMenuAction(bookingsMenu)));

        mainMenu.add(VerticalSeparator.separator());

        final Menu accountMenu = buildAccountMenu();
        mainMenu.add(new SubMenu(ACCOUNT_OPTION, accountMenu, new ShowVerticalSubMenuAction(accountMenu)));

        mainMenu.add(VerticalSeparator.separator());

        final Menu ratingsMenu = buildRatingsMenu();
        mainMenu.add(new SubMenu(RATINGS_OPTIONS, ratingsMenu, new ShowVerticalSubMenuAction(ratingsMenu)));
        // TODO add menu options

        mainMenu.add(VerticalSeparator.separator());

        mainMenu.add(new MenuItem(EXIT_OPTION, "Exit", new ExitWithMessageAction()));

        return mainMenu;
    }

    private Menu buildAccountMenu() {
        final Menu menu = new Menu("Account");
        menu.add(new MenuItem(LIST_MOVEMENTS_OPTION, "List movements", new ShowMessageAction("Not implemented yet")));
        menu.add(new MenuItem(CHANGE_NUTRITIONAL_INFO, "Change nutritional information", new ChangeNutritionalProfileAction()));
        menu.add(new MenuItem(CHANGE_USER_ALLERGENS, "Change user allergens", new ChangeUserAllergensAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
        return menu;
    }

    private Menu buildBookingsMenu() {
        final Menu menu = new Menu("Bookings");
        menu.add(new MenuItem(LIST_MENUS_OPTION, "List menus", new ShowMessageAction("Not implemented yet")));
        menu.add(new MenuItem(BOOK_A_MEAL_OPTION, "Book a meal", new MakeReservationRequestAction()));
        menu.add(new MenuItem(CANCEL_BOOKING_OPTION, "Cancel Existing Reservation", new CancelExistingReservationsAction()));
        menu.add(new MenuItem(LIST_THIS_WEEK_MENUS_OPTION, "List this week menus", new ListWeeklyMenuAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
        return menu;
    }

    private Menu buildRatingsMenu() {
        final Menu menu = new Menu("Ratings");
        menu.add(new MenuItem(RATE_MEAL_OPTION, "Rate a meal", new RateMealAction()));
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));
        return menu;
    }

    @Override
    protected CafeteriaUserBaseController controller() {
        return new CafeteriaUserBaseController();
    }

    public void createBalanceWatchDog() {
        BalanceWatchDog balenceWatchDog = controller().balanceWatchDog();
        balenceWatchDog.addObserver(this);
    }

    @Override
    public void update(Observable o, Object o1) {
        System.out.println("WARNING: Your balance is currently below the average meal price this week.");
    }

}
