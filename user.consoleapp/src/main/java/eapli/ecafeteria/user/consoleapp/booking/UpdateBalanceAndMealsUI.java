/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.booking;

import eapli.ecafeteria.application.booking.UpdateBalanceAndMealsController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.domain.Money;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gustavo Gonçalves (1140320), Vasco Lusitano (1140338)
 */
public class UpdateBalanceAndMealsUI extends AbstractUI {

    
    Meal meal;
    UpdateBalanceAndMealsController controller = new UpdateBalanceAndMealsController();

    public UpdateBalanceAndMealsUI(Meal meal) {
        this.meal = meal;
    }

    @Override
    protected boolean doShow() {

        Money m = null;
        try {
            m = controller.updateBalanceAndMeals(meal);
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(UpdateBalanceAndMealsUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(UpdateBalanceAndMealsUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        double balance = m.amount();
        
        System.out.printf("Saldo atualizado!\nO seu saldo atual é %f €\n", balance);

        return true;
    }

    @Override
    public String headline() {
        return "Update Balance and Number of Meals";
    }
}
