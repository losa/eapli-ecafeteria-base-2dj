/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.booking;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.application.booking.CancelReservationController;
import eapli.ecafeteria.domain.authz.SystemUser;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.cafeteria.OrganicUnit;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.domain.user.Booking;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;
import java.util.ArrayList;
import static java.util.Collections.copy;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1150371
 */
public class CancelExistingReservationsUI extends AbstractUI {

    private final CancelReservationController theController = new CancelReservationController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        Iterable<Booking> availableReservations = theController.bookingsByUser();
        int index = 1;

        System.out.println("List of non canceled reservations (already canceled were already filtered)");
        for (Booking availableReservation : availableReservations) {
            if (availableReservation.bookingUndelivered()
                    && !availableReservation.cancelBooking()) {
                System.out.println(index + ")------> RESERVATION: - " + availableReservation.toString());
            } else {
            }
            index++;
        }

        int selectedIndex = Console.readInteger("\nSelect a Reservation!");
        List<Booking> array = new ArrayList<>();
        Iterator iterator = availableReservations.iterator();
        while (iterator.hasNext()) {
            Booking e = (Booking) iterator.next();
            array.add(e);
        }

        Booking r = array.get(selectedIndex - 1);

        String userConfirmation = "";
        do {
            userConfirmation = Console.readLine("Do you really want to cancel reservation? (yes/no)");
        } while (!userConfirmation.equalsIgnoreCase("yes") && !userConfirmation.equalsIgnoreCase("no"));

        if (userConfirmation.equalsIgnoreCase("yes")) {
            try {

                theController.setBooking(r);
                theController.cancelBooking();

                System.out.println("Reservations (" + r.toString() + ") canceled with success!!");
            } catch (DataIntegrityViolationException ex) {
                Logger.getLogger(CancelExistingReservationsUI.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("ERROR");
                return false;
            } catch (DataConcurrencyException ex) {
                Logger.getLogger(CancelExistingReservationsUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("Nothing as been done.");
        }

        return true;
    }

    @Override
    public String headline() {
        return "Booking Cancelation sucess!";
    }

}
