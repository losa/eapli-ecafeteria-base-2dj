/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.ListWeeklyMenuController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MenuPrinter;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractListUI;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author vasco
 */
public class ListWeeklyMenuUI extends AbstractListUI<Menu> {

    private final ListWeeklyMenuController theController = new ListWeeklyMenuController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected Iterable<Menu> listOfElements() {
        return this.theController.allDishes();
    }

    @Override
    protected Visitor<Menu> elementPrinter() {
        return new MenuPrinter();
    }

    @Override
    protected String elementName() {
        return "Menu";
    }
}
