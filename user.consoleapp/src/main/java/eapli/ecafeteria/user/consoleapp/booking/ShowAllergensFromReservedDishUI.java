/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.booking;

import eapli.ecafeteria.application.booking.ShowAllergensFromReservedDishController;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.presentation.console.AbstractUI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Vasco Lusitano(1140338) e Gustavo Gonçalves(1140320)
 */
public class ShowAllergensFromReservedDishUI extends AbstractUI {

    Meal meal;
    ShowAllergensFromReservedDishController controller = new ShowAllergensFromReservedDishController();

    public ShowAllergensFromReservedDishUI(Meal meal) {
        this.meal = meal;
    }

    @Override
    protected boolean doShow() {

        //Map<Allergen, Boolean> m = controller.dishAllergensAndCommonUserAllergens(meal.dishFromMeal());

        List<Allergen> lista = new ArrayList<>();

        /**for (Map.Entry<Allergen, Boolean> entry : m.entrySet()) {
            if (entry.getValue() == true) {
                lista.add(entry.getKey());
            }
        }*/

        if (lista.isEmpty()) {
            System.out.println("Não há alergénios.");
        } else {
            for (Allergen a : lista) {
                System.out.println(a.name());
            }
        }

        ShowNutritionalInformationAndWeeklyCaloricIntakeUI sniawci = new ShowNutritionalInformationAndWeeklyCaloricIntakeUI(meal);
        sniawci.doShow();
        
        return true;
    }

    @Override
    public String headline() {
        return "Show Allergens From Reserved Dish";
    }
}
