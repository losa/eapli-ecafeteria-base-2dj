/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.booking;

import eapli.ecafeteria.application.booking.MakeReservationRequestController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MealPrinter;

/**
 * Vasco Lusitano(1140338) e Gustavo Gonçalves(1140320)
 */
public class MakeReservationRequestUI extends AbstractUI {

    private final MakeReservationRequestController controller = new MakeReservationRequestController();

    @Override
    protected boolean doShow() {

        /* Meal section */
        Calendar date = Console.readCalendar("Insert meal date: (dd-MM-yyyy)");

        /* Meal section */
        final List<Meal> meal = this.controller.mealsPlannedByDate(date);

        if (!meal.iterator().hasNext()) {
            System.out.println("There no meal defined!");
            return false;
        }

        final SelectWidget<Meal> mealSelector = new SelectWidget<>(" a mudar", meal, new MealPrinter());
        mealSelector.show();

        final Meal theMeal = mealSelector.selectedElement();

        if (theMeal == null) {
            System.out.println("There are no meals available!");
            return false;
        } else {
            try {
                if (this.controller.createBooking(theMeal) == null) {
                    System.out.println("Booking not registered.");
                } else {
                    System.out.println("Booking registered.");
                }
            } catch (DataConcurrencyException ex) {
                Logger.getLogger(MakeReservationRequestUI.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Data was changed meanwhile. Please try again.");
            } catch (DataIntegrityViolationException ex) {
                Logger.getLogger(MakeReservationRequestUI.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Reservation already registered!");
            }
        }

        ShowAllergensFromReservedDishUI safrd = new ShowAllergensFromReservedDishUI(theMeal);
        safrd.doShow();
        
        return false;
    }

    @Override
    public String headline() {
        return "Book Meal";
    }
}
