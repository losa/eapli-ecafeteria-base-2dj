/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.booking;

import eapli.framework.actions.Action;

/**
 *
 * @author gustavo (1140320)
 */
public class MakeReservationRequestAction implements Action {

    @Override
    public boolean execute() {
        return new MakeReservationRequestUI().show();
    }
}
