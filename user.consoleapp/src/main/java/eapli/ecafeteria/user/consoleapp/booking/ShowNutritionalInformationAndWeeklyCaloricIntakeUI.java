/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.booking;

import eapli.ecafeteria.application.booking.ShowNutritionalInformationAndWeeklyCaloricIntakeController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.NutricionalInfo;
import eapli.framework.presentation.console.AbstractUI;

/**
 * Vasco Lusitano(1140338) e Gustavo Gonçalves(1140320)
 */
public class ShowNutritionalInformationAndWeeklyCaloricIntakeUI extends AbstractUI {

    private final ShowNutritionalInformationAndWeeklyCaloricIntakeController controller = new ShowNutritionalInformationAndWeeklyCaloricIntakeController();

    Meal meal;

    public ShowNutritionalInformationAndWeeklyCaloricIntakeUI(Meal meal) {
        this.meal = meal;
    }

    @Override
    protected boolean doShow() {

        NutricionalInfo ni = controller.dishNutritionalInfo(meal.dishFromMeal());

        System.out.printf("Calorias: %d\n", ni.calories());
        System.out.printf("Sal: %d\n", ni.salt());

        int calorias = controller.calculateNewCaloricIntake(ni);

        System.out.printf("Weekly caloric intake: %d\n", calorias);

        UpdateBalanceAndMealsUI ubamui = new UpdateBalanceAndMealsUI(meal);
        ubamui.doShow();
        
        return true;
    }

    @Override
    public String headline() {
        return "Show Nutritional Information And Weekly Caloric Intake";
    }
}
