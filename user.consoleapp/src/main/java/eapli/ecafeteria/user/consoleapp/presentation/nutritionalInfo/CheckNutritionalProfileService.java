/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.nutritionalInfo;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.NutritionalProfile;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author Rui Peixoto
 */
public class CheckNutritionalProfileService {
    
    private final CafeteriaUserRepository repository = PersistenceContext.repositories().cafeteriaUsers(null);
    
    public NutritionalProfile getNutritionalProfile() {

        CafeteriaUser cafeteriaUser = repository.findByUsername(Application.session().session().authenticatedUser().username());

        return cafeteriaUser.nutritionalProfile();
    }
}
