/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.nutritionalInfo;

import eapli.ecafeteria.application.cafeteria.ChangeUserAllergensController;
import eapli.ecafeteria.application.meals.ListAllergenService;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.AllergensPrinter;
import eapli.ecafeteria.domain.cafeteria.UserAllergen;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ListWidget;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.Collections;
import eapli.util.io.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rui Peixoto
 */
public class ChangeUserAllergensUI extends AbstractUI {

    private final ChangeUserAllergensController controller = new ChangeUserAllergensController();

    protected Controller controller() {
        return this.controller;
    }

    @Override
    protected boolean doShow() {
        final ListWidget<UserAllergen> selectorUserAllergens = new ListWidget<>("User Allergens:",
                this.controller.getUserAllergens(), new UserAllergensPrinter());
        selectorUserAllergens.show();
        System.out.println("");

        System.out.println("1. Add Allergen");
        System.out.println("2. Delete Allergen");
        System.out.println("0. Exit");
        
        final int option = Console.readOption(1, 2, 0);
        System.out.println("");
        try {
            switch (option) {
                case 1:
                    final SelectWidget<Allergen> selectorAllergens = new SelectWidget<>("All Allergens:",
                            new ListAllergenService().allAllergens(), new AllergensNamePrinter());
                    selectorAllergens.show();
                    final Allergen allergenSelected = selectorAllergens.selectedElement();
                    
                    if(allergenSelected == null){
                        break;
                    }
                    
                    if (controller.addNewAllergen(allergenSelected) == null) {
                        System.out.println("Selected allergen is already on the list");
                    }

                    break;
                case 2:
                    final SelectWidget<UserAllergen> selectorUserAllergen = new SelectWidget<>("Select one allergen to delete",
                            this.controller.getUserAllergens(), new UserAllergensPrinter());
                    selectorUserAllergen.show();
                    final UserAllergen userAllergenSelected = selectorUserAllergen.selectedElement();
                    
                    if(userAllergenSelected != null){
                        controller.deleteAllergen(userAllergenSelected);
                    }     
                    
                    break;
                case 0:
                    break;
                default:
                    System.out.println("No valid option selected");
                    break;
            }
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(ChangeUserAllergensUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(ChangeUserAllergensUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    @Override
    public String headline() {
        return "Change User Allergens:";
    }

}
