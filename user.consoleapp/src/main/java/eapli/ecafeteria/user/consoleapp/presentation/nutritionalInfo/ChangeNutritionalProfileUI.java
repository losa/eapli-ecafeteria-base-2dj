package eapli.ecafeteria.user.consoleapp.presentation.nutritionalInfo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import eapli.ecafeteria.application.cafeteria.ChangeNutritionalProfileController;
import eapli.ecafeteria.domain.cafeteria.NutritionalProfile;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rui Peixoto
 */
public class ChangeNutritionalProfileUI extends AbstractUI {

    private final ChangeNutritionalProfileController controller = new ChangeNutritionalProfileController();

    protected Controller controller() {
        return this.controller;
    }

    @Override
    protected boolean doShow() {
        final NutritionalProfile nutritional = controller.getActualNutritionalProfile();

        if (nutritional != null) {
            System.out.println("Current nutritional information:\n" + nutritional.toString());
        } else {
            System.out.println("There are no saved values for nutritional profile!\n");
        }

        final int maxCaloriesPerMeal = Console.readInteger("New value to maximum calories per meal");
        final int maxSaltPerMeal = Console.readInteger("New value to maximum salt per meal");
        final int maxCaloriesPerWeek = Console.readInteger("New value to maximum calories per week");
        final int maxSaltPerWeek = Console.readInteger("New value to maximum salt per week");

        try {
            controller.changeNutritionalProfile(maxCaloriesPerMeal, maxCaloriesPerWeek, maxSaltPerMeal, maxSaltPerWeek);
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            Logger.getLogger(ChangeNutritionalProfileUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }

    @Override
    public String headline() {
        return "Change Nutritional Profile:";
    }

}
