/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.ratings;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class UserMealPrinter implements Visitor<Meal> {

    @Override
    public void visit(Meal visitee) {
        System.out.printf("%-30s%-25s%-10s\n", visitee.date().getTime(), visitee.dishName(), visitee.mealType());
    }
}
