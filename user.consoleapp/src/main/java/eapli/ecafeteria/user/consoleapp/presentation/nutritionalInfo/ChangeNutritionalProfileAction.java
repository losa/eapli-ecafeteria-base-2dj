package eapli.ecafeteria.user.consoleapp.presentation.nutritionalInfo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import eapli.framework.actions.Action;

/**
 *
 * @author Rui Peixoto
 */
public class ChangeNutritionalProfileAction implements Action{

    @Override
    public boolean execute() {
       return new ChangeNutritionalProfileUI().show();
    }
    
}
