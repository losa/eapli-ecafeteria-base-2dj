/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.user.consoleapp.presentation.ratings;

import eapli.ecafeteria.application.users.RateMealController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.user.Rating;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;

/**
 *
 * @author Miguel Minnemann (1150457) & Daniel Ribeiro (1150577)
 */
public class RateMealUI extends AbstractUI {

    private final RateMealController theController = new RateMealController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final Iterable<Meal> meals = this.theController.userMeals();
      
        final SelectWidget<Meal> selector = new SelectWidget<>("Select the meal to rate", meals, new UserMealPrinter());
        selector.show();
        final Meal theMealSelected = selector.selectedElement();
        
        if(theMealSelected == null) {
            return true;
        }

        final int rating = Console.readInteger("Rating (1 to 5 stars)");

        try {
            Rating ratingCreated = this.theController.registerRating(rating, theMealSelected);
            System.out.println("Do you wish to add a comment to your rating?\n 1- Yes \n 2- No");
            final int option = Console.readOption(1, 2, 2);
            if(option == 1) {
                String comment = Console.readLine("Please, enter a comment:");
                this.theController.addCommentToRating(ratingCreated, comment);
            }
            return true;
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            System.out.println("This meal was already rated by you");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Rate Meal";
    }
}
