package eapli.ecafeteria.domain.meals;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.NutricionalInfo;
import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author AntónioRocha
 */
public class DishTest {

    private DishType peixe;
    private NutricionalInfo aNutricionalInfo;
    private final Designation prego = Designation.valueOf("Prego");
    ArrayList<Allergen> list = new ArrayList<>();
    //private final DishAllergen dishAllergen = new DishAllergen(list);

    public DishTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        peixe = new DishType("Peixe", "Peixe");
        aNutricionalInfo = new NutricionalInfo(10, 11);
    }

    @After
    public void tearDown() throws Exception {
    }
/*
    @Test(expected = IllegalStateException.class)
    public void testDishTypeMustNotBeNull() {
        System.out.println("must have an Dish type");
        Dish instance = new Dish(null, prego, aNutricionalInfo, dishAllergen, Money.euros(8));
    }

        
    @Test(expected = IllegalStateException.class)
    public void testNameMustNotBeNull() {
        System.out.println("must have an name");
        Dish instance = new Dish(peixe, null, aNutricionalInfo, dishAllergen, Money.euros(5));
    }

    @Test(expected = IllegalStateException.class)
    public void testNutricionalInfoMustNotBeNull() {
        System.out.println("must have an Nutricional Info");
        Dish instance = new Dish(peixe, prego, null, dishAllergen, Money.euros(7));
    }

    @Test(expected = IllegalStateException.class)
    public void testDishAllergensMustNotBeNull() {
        System.out.println("must have an Nutricional Info");
        Dish instance = new Dish(peixe, prego, aNutricionalInfo, null, Money.euros(7));
    }

    /**
     * Test of is method, of class Dish.
     */
 /*   @Test
    public void testIs() {
        System.out.println("Attest 'is' method - Normal Behaviour");
        Dish instance = new Dish(peixe, prego, aNutricionalInfo, dishAllergen, Money.euros(5));
        boolean expResult = true;
        boolean result = instance.is(prego);
        assertEquals(expResult, result);
    }
*/
    /**
     * Test of changeNutricionalInfoTo method, of class Dish.
     *
     * PRP - 29.mar.2017
     */
 /*   @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangeNutricionalInfoToNull() {
        System.out.println("ChangeNutricionalInfoTo -New nutricional info must not be null");

        final Dish Dishinstance = new Dish(peixe, prego, new NutricionalInfo(1, 1),
                dishAllergen, Money.euros(7));
        Dishinstance.changeNutricionalInfoTo(null);
    }
// errros em vários teste que comentei JAL 
    /**
     * Tests of changePriceTo method, of class Dish.
     *
     * PRP - 29.mar.2017
     */
/*    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangePriceToNull() {
        System.out.println("ChangePriceTo -New price info must not be null");

        final Dish Dishinstance = new Dish(peixe, prego, new NutricionalInfo(1, 1),
                dishAllergen, Money.euros(7));
        Dishinstance.changePriceTo(null);
    }
*/
    /*
    @Test(expected = IllegalArgumentException.class)
    public void ensureCannotChangePriceToNegative() {
        System.out.println("ChangePriceTo -New price can nt be negativel");

        final Dish Dishinstance = new Dish(peixe, prego, new NutricionalInfo(1, 1), dishAllergen,
                Money.euros(1));
        Dishinstance.changePriceTo(Money.euros(-1));
    }
*/
}
