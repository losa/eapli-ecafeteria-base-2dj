/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import eapli.framework.domain.TimePeriod2;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlos
 */
public class MenuTest {
    
    public MenuTest() {
    }
    
    private TimePeriod2 timePeriod;
    
    
    @Before
    public void setUp() {
        Calendar initialDate = DateTime.now();
        Calendar finalDate = DateTime.now();
        finalDate.add(Calendar.DAY_OF_MONTH, 2);
        
        timePeriod = new TimePeriod2(initialDate, finalDate);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureDescriptionCantBeNull() {
        System.out.println("Description in a menu cannot be null");
        Menu menu = new Menu(null, timePeriod);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensurePeriodCantBeNull() {
        System.out.println("Period in a menu cannot be null");
        Menu menu = new Menu("Ementa teste", null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureInitialDateCantBeBeforeFinalDate() {
        System.out.println("Initial date on menu can´t be after final date");
        
        Calendar initialDate = DateTime.now();
        Calendar finalDate = DateTime.now();
        finalDate.add(Calendar.DAY_OF_MONTH, 2);
        
        TimePeriod2 timePeriod2 = new TimePeriod2(finalDate, initialDate);
        
        Menu menu = new Menu("Ementa teste", timePeriod2);
    }
}
