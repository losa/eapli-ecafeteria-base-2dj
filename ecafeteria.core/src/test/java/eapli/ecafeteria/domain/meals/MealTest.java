/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import eapli.framework.domain.TimePeriod2;
import eapli.util.DateTime;
import java.util.Calendar;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class MealTest {

    public MealTest() {
    }

    private Dish dish;
    private MealType mealType;
    private Menu menu;

    @Before
    public void setUp() {
        DishType dishType = new DishType("Test", "Dish Type for tests");
        dish = new Dish(dishType, Designation.valueOf("Test"), Money.euros(5));
        mealType = new MealType("Test");
        
        Calendar initialDate = DateTime.now();
        Calendar finalDate = DateTime.now();
        finalDate.add(Calendar.DAY_OF_MONTH, 2);
        
        menu = new Menu("Ementa 1", new TimePeriod2(initialDate, finalDate));
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureDishCantBeNull() {
        System.out.println("Dish in a meal cannot be null");
        
        Meal meal = new Meal(null, mealType, DateTime.now(), menu);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureMealTypeCantBeNull() {
        System.out.println("Meal type in a meal cannot be null");
        Meal meal = new Meal(dish, null, DateTime.now(), menu);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureDateCantBeBeforeToday() {
        System.out.println("Date on meal can't be before today's date");
        Meal meal = new Meal(dish, mealType, DateTime.beginningOfWeek(2000, 20), menu);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureMenuCantBeNull() {
        System.out.println("Menu on meal cannot be null");
        Meal meal = new Meal(dish, mealType, DateTime.now(), null);
    }
}
