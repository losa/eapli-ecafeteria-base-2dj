/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import org.junit.Test;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class MealTypeTest {

    public MealTypeTest() {
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNameCantBeEmpty() {
        System.out.println("Mealtype name cannot be empty");
        MealType mealType = new MealType("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureNameCantBeNull() {
        System.out.println("Mealtype name cannot be null");
        MealType mealType = new MealType(null);
    }

}
