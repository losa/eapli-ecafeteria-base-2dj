/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain;

import eapli.ecafeteria.domain.user.Comment;
import org.junit.Test;

/**
 *
 * @author Daniel
 */
public class CommentTest {

    @Test(expected = IllegalArgumentException.class)
    public void ensureCommentCantBeNull() {
        System.out.println("Comment can't be null!");
        Comment comment = new Comment(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void ensureCommentCantBeEmpty() {
        System.out.println("Comment can't be empty!");
        Comment comment = new Comment("");
    }
}
