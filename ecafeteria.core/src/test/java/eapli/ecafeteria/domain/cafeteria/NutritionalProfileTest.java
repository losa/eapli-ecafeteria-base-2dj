/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Rui Peixoto
 */
public class NutritionalProfileTest {

    public NutritionalProfileTest() {
    }

    /**
     * Test of calories per week method, of class NutritionalProfile.
     */
    @Test(expected = IllegalStateException.class)
    public void testCaloriesPerWeekMustNotBeNull() {
        System.out.println("calories per week must not be null");
        NutritionalProfile instance = new NutritionalProfile(10,5,null,50);
    }

    /**
     * Test of calories per week method, of class NutritionalProfile.
     */
    @Test(expected = IllegalStateException.class)
    public void testCaloriesPerWeekMustNotBeNegative() {
        System.out.println("calories per week must not be negative");
        NutritionalProfile instance = new NutritionalProfile(10,5,-5,50);
    }
    
    /**
     * Test of calories per meal method, of class NutritionalProfile.
     */
    @Test(expected = IllegalStateException.class)
    public void testCaloriesPerMealMustNotBeNull() {
        System.out.println("calories per meal must not be null");
        NutritionalProfile instance = new NutritionalProfile(null,5,90,50);
    }
    
    /**
     * Test of calories per meal method, of class NutritionalProfile.
     */
    @Test(expected = IllegalStateException.class)
    public void testCaloriesPerMealMustNotBeNegative() {
        System.out.println("calories per meal must not be negative");
        NutritionalProfile instance = new NutritionalProfile(-5,5,90,50);
    }

     /**
     * Test of salt per week method, of class NutritionalProfile.
     */
    @Test(expected = IllegalStateException.class)
    public void testSaltPerWeekMustNotBeNull() {
        System.out.println("salt per week must not be null");
        NutritionalProfile instance = new NutritionalProfile(30,5,90,null);
    }

    /**
     * Test of calories method, of class NutritionalProfile.
     */
    @Test(expected = IllegalStateException.class)
    public void testSaltPerWeekMustNotBeNegative() {
        System.out.println("salt per week must not be null");
        NutritionalProfile instance = new NutritionalProfile(30, 5, 90, -5);
    }

     /**
     * Test of calories method, of class NutritionalProfile.
     */
    @Test(expected = IllegalStateException.class)
    public void testSaltPerMealMustNotBeNull() {
        System.out.println("salt per meal must not be null");
        NutritionalProfile instance = new NutritionalProfile(30,null,90,50);
    }
    
     /**
     * Test of calories method, of class NutritionalProfile.
     */
    @Test(expected = IllegalStateException.class)
    public void testSaltPerMealMustNotBeNegative() {
        System.out.println("salt per meal must not be negative");
        NutritionalProfile instance = new NutritionalProfile(30,-5,90,50);
    }
    
}
