/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain;

import eapli.ecafeteria.domain.user.Rating;
import eapli.ecafeteria.domain.authz.RoleType;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.domain.Designation;
import eapli.framework.domain.Money;
import eapli.framework.domain.TimePeriod2;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Miguel Minnemann (1150457) & Daniel Ribeiro (1150577)
 */
public class RatingTest {

    public RatingTest() {
    }

    private SystemUser user;
    private Meal meal;

    @Before
    public void setUp() {
        Set<RoleType> roleTypes = new HashSet<>();
        roleTypes.add(RoleType.CAFETERIA_USER);
        user = new SystemUser("Teste", "Password1", "Teste", "Teste", "teste@isep.ipp.pt", roleTypes);
        DishType dishType = new DishType("Teste", "Tipo de pratos para testes");
        Dish dish = new Dish(dishType, Designation.valueOf("Prato de Teste"), Money.euros(5));
        MealType mealType = new MealType("Teste");
        
        Calendar initialDate = DateTime.now();
        Calendar finalDate = DateTime.now();
        finalDate.add(Calendar.DAY_OF_MONTH, 2);
        Menu menu = new Menu("Ementa 1", new TimePeriod2(initialDate, finalDate));
        
        meal = new Meal(dish, mealType, DateTime.now(), menu);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureRatingCantBeAboveFive() {
        System.out.println("Stars given can't be above five");
        Rating rating = new Rating(6, meal, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureRatingCantBeBelowOne() {
        System.out.println("Stars given can't be below one");
        Rating rating = new Rating(0, meal, user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureRatingUserCantBeNull() {
        System.out.println("User can't be null");
        Rating rating = new Rating(4, meal, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureRatingMealCantBeNull() {
        System.out.println("Meal can't be null");
        Rating rating = new Rating(3, null, user);
    }

}
