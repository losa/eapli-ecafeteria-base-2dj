/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.complaints;

import eapli.ecafeteria.domain.complaints.Complaint;
import eapli.ecafeteria.Application;
import eapli.ecafeteria.application.meals.ListDishService;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.persistence.ComplaintRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author gustavo (1140320), Vasco (1140338)
 */
public class RegisterComplaintController implements Controller {

    private ListDishService svc = new ListDishService();
    private final ComplaintRepository complaintRepository = PersistenceContext.repositories().complaints();

    public Iterable<Dish> getDishes() {
        //Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        return svc.allDishes();
    }

    public CafeteriaUser getCafetariaUserByMecanographicNumber(String mecanographicNumber){
        MecanographicNumber mn = new MecanographicNumber(mecanographicNumber);
        
        CafeteriaUser cafeUser = PersistenceContext.repositories().cafeteriaUsers(null).findByMecanographicNumber(mn);
    
        return cafeUser;
    }
    
    public Complaint registerComplaint(String complaint, Dish dish, CafeteriaUser user)
            throws DataIntegrityViolationException, DataConcurrencyException {
        //Application.ensurePermissionOfLoggedInUser(ActionRight.ADMINISTER);

        return this.complaintRepository.save(new Complaint(complaint, dish, user));
    }

}
