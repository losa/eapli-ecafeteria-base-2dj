/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.application;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.BalanceWatchDog;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.UserCard;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.domain.Money;

/**
 *
 * @author mcn
 */
public class CafeteriaUserBaseController implements Controller {

    private CafeteriaUserRepository cafeteriaUserRepository = PersistenceContext.repositories().cafeteriaUsers(null);

    public Money balance() {
        SystemUser userLoggedOn = Application.session().session().authenticatedUser();
        CafeteriaUser cafeteriaUser = cafeteriaUserRepository.findByUsername(userLoggedOn.id());
        return cafeteriaUser.userCard().balance();
    }

    public BalanceWatchDog balanceWatchDog() {
        SystemUser userLoggedOn = Application.session().session().authenticatedUser();
        CafeteriaUser cafeteriaUser = cafeteriaUserRepository.findByUsername(userLoggedOn.id());
        UserCard userCard = cafeteriaUser.userCard();
        BalanceWatchDog balanceWatchDog = new BalanceWatchDog();
        userCard.addObserver(balanceWatchDog);
        return balanceWatchDog;
    }
}
