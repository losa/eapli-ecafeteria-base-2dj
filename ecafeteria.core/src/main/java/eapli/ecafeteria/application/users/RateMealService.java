/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.users;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.user.Rating;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.user.Booking;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RatingRepository;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Miguel Minnemann (1150457) & Daniel Ribeiro (1150577)
 */
public class RateMealService {

    private BookingRepository bookingRepository = PersistenceContext.repositories().bookings();
     private CafeteriaUserRepository cafeteriaUserRepository = PersistenceContext.repositories().cafeteriaUsers(null);
    private RatingRepository ratingRepository = PersistenceContext.repositories().ratings();
    
    public Iterable<Meal> allMealsByUser() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);
    
        SystemUser userLoggedOn = Application.session().session().authenticatedUser();
        final CafeteriaUser cafeteriaUser = cafeteriaUserRepository.findByUsername(userLoggedOn.id());
        Iterable<Booking> bookingsFromUser = bookingRepository.allBookingsDeliveredToUser(cafeteriaUser);
        List<Meal> mealsToRate = new LinkedList<>();
        Iterable<Rating> ratingsFromUser = ratingRepository.findAllRatingsByUser(userLoggedOn);
        List<Meal> mealsRatedByUser = new LinkedList<>();
        
        for (Rating rating : ratingsFromUser) {
            mealsRatedByUser.add(rating.meal());
        }
        
       for(Booking booking : bookingsFromUser) {
            mealsToRate.add(booking.meal());
        }
        
        for(Meal mealToRate : mealsToRate) {
            for(Meal mealRated : mealsRatedByUser) {
                if(mealRated.equals(mealToRate)) {
                    mealsToRate.remove(mealRated);
                    break;
                }
            }
        }
        
        return mealsToRate;
    }
}
