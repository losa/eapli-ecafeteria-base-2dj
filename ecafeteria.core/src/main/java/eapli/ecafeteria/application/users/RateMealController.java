/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.users;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.user.Comment;
import eapli.ecafeteria.domain.user.Rating;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RatingRepository;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Miguel Minnemann (1150457) & Daniel Ribeiro (1150577)
 */
public class RateMealController implements Controller {

    private RatingRepository ratingRepository = PersistenceContext.repositories().ratings();
    private CafeteriaUserRepository cafeteriaUserRepository = PersistenceContext.repositories().cafeteriaUsers(null);
    private RateMealService svc = new RateMealService();

    public Iterable<Meal> userMeals() {
        return svc.allMealsByUser();
    }

    public Rating registerRating(final int numeroEstrelas, final Meal meal)
            throws DataIntegrityViolationException, DataConcurrencyException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);

        final SystemUser userLoggedOn = Application.session().session().authenticatedUser();
        
        Rating newRating = new Rating(numeroEstrelas, meal, userLoggedOn);
        return ratingRepository.save(newRating);
    }

    public Rating addCommentToRating(final Rating rating, final String comment)
            throws DataIntegrityViolationException, DataConcurrencyException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.SELECT_MEAL);

        rating.addComment(new Comment(comment));

        return ratingRepository.save(rating);
    }

}
