package eapli.ecafeteria.application.booking;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.user.Booking;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Mariana Calafate(1150481), Manuel Gerardo(1150480),
 * Vasco Lusitano(1140338) e Gustavo Gonçalves(1140320)
 */
public class MakeReservationRequestController implements Controller {
   
    public Calendar calendar(int day, int month, int year) {
        return DateTime.newCalendar(year, month, day);
    }
    
    public List<Meal> mealsPlannedByDate(Calendar date){
        return (List<Meal>) PersistenceContext.repositories().meals().findByDate(date);
    }
    
    public Booking createBooking(Meal meal) throws DataConcurrencyException, DataIntegrityViolationException {
        final SystemUser userLoggedOn = Application.session().session().authenticatedUser();
        CafeteriaUser cafeUser = PersistenceContext.repositories().cafeteriaUsers(null).findByUsername(userLoggedOn.username());
        return PersistenceContext.repositories().bookings().save(new Booking(meal, cafeUser));
    }

}
