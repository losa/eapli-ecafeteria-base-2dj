/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.booking;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.UserAllergen;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishAllergen;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.Map;

/**
 *
 * @author Mariana Calafate(1150481@isep.ipp.pt)
 */
public class ShowAllergensFromReservedDishController {
    
    public Map<Allergen, Boolean> dishAllergensAndCommonUserAllergens(Dish dish){
        final SystemUser userLoggedOn = Application.session().session().authenticatedUser();
        CafeteriaUser cafeUser = PersistenceContext.repositories().cafeteriaUsers(null).findByUsername(userLoggedOn.username());
        DishAllergen dishAllergens = dish.dishAllergens();
        UserAllergen userAllergens = cafeUser.userAllergens();
        return dishAllergens.checkUserAllergen(userAllergens);
    }
}
