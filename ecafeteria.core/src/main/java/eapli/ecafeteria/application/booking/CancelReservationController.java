/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.booking;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.user.Booking;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.List;

/**
 *
 * @author luis
 */
public class CancelReservationController implements Controller {
    
    private Booking cancel;
    
    public List<Booking> bookingsByUser(){
        final SystemUser userLoggedOn = Application.session().session().authenticatedUser();
        CafeteriaUser cafeUser = PersistenceContext.repositories().cafeteriaUsers(null).findByUsername(userLoggedOn.username()); 
        return (List<Booking>) PersistenceContext.repositories().bookings().findAllBookingsByUser(cafeUser);
    }
    
    public void setBooking(Booking booking){
        this.cancel=booking;
    }
    
    public void cancelBooking() throws DataIntegrityViolationException, DataConcurrencyException{
        cancel.cancelBooking();
        PersistenceContext.repositories().bookings().save(cancel);
    }
    
    
}
