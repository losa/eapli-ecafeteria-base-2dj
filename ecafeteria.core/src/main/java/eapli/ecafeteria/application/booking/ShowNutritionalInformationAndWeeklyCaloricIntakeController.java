/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.booking;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.NutricionalInfo;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author manuelgerardo
 */
public class ShowNutritionalInformationAndWeeklyCaloricIntakeController {
    
    public NutricionalInfo dishNutritionalInfo(Dish dish){
        return dish.nutricionalInfo();
    }
    
    public int calculateNewCaloricIntake(NutricionalInfo nutInfo){
        final SystemUser userLoggedOn = Application.session().session().authenticatedUser();
        CafeteriaUser cafeUser = PersistenceContext.repositories().cafeteriaUsers(null).findByUsername(userLoggedOn.username());
        return nutInfo.calculateNewCaloricIntake(cafeUser.weeklyCaloricIntake());
    }
}
