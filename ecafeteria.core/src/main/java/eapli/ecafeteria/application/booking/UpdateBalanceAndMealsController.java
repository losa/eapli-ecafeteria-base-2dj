/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.booking;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.CardMovement;
import eapli.ecafeteria.domain.cafeteria.MovementType;
import eapli.ecafeteria.domain.cafeteria.UserCard;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.domain.Money;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Gustavo Gonçalves (1140320), Vasco Lusitano (1140338)
 */
public class UpdateBalanceAndMealsController {
    
    CafeteriaUserRepository cafeteriaUserRepository = PersistenceContext.repositories().cafeteriaUsers(null);
    
    public Money updateBalanceAndMeals(Meal meal) throws DataConcurrencyException, DataIntegrityViolationException{
        
        final SystemUser userLoggedOn = Application.session().session().authenticatedUser();
        CafeteriaUser cafeUser = cafeteriaUserRepository.findByUsername(userLoggedOn.username());
        
        Money m = meal.dishFromMeal().currentPrice();
        CardMovement cm = new CardMovement(MovementType.PURCHASE, m, cafeUser);
        UserCard uc = cafeUser.userCard();
        uc.addMovement(cm);
        
        Money balanceUser = uc.balance();
        
        cafeteriaUserRepository.save(cafeUser);
        
        return balanceUser;
        // falta parte de atualizar numero de refeições
    }
    
}
