/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cashier;

import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.domain.Designation;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author manuelgerardo
 */
public class ViewAvailableMealsService {
    
    public Map<Designation, Integer> mealTypeForThisSession(POS pos){
        if (pos==null){
             return null;
         }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(pos.DateToWork());
        Iterable<MealPlanItem> mpil = PersistenceContext.repositories().mealPlanItems().findMealPlanItemByDateAndType(calendar, pos.MealToWork());
        return availableQuantityByMeal(mpil);
    }
    
    private Map<Designation, Integer> availableQuantityByMeal(Iterable<MealPlanItem> mealPlans){
        Map<Designation,Integer> availableQuantityByMeal = new HashMap<>();
        for(MealPlanItem mealPlanItem : mealPlans){
            availableQuantityByMeal.put(mealPlanItem.meal().dishName(), mealPlanItem.amount());
        }
        return availableQuantityByMeal;
    }
    
}
