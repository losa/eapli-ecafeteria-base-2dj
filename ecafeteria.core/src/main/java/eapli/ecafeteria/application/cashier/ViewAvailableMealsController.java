/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cashier;

import eapli.ecafeteria.domain.authz.UserSession;
import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.framework.application.Controller;
import eapli.framework.domain.Designation;
import java.util.Map;

/**
 *
 * @author manuelgerardo
 */
public class ViewAvailableMealsController implements Controller {
    
    public Map<Designation, Integer> mealsPlannedByDate(){
         POS pos = UserSession.getPOS();
         if (pos==null){
             return null;
         }
         return new ViewAvailableMealsService().mealTypeForThisSession(pos);
    }
    
}
