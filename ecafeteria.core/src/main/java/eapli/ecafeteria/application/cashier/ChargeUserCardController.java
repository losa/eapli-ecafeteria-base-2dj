/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cashier;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.CardMovement;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.cafeteria.MovementType;
import eapli.ecafeteria.domain.cafeteria.UserCard;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.domain.Money;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class ChargeUserCardController implements Controller {

    private CafeteriaUserRepository cafeteriaUserRepository = PersistenceContext.repositories().cafeteriaUsers(null);

    public CafeteriaUser addCredit(final String mecanographicNumber, final double money)
            throws DataConcurrencyException, DataIntegrityViolationException {
        Application.session().ensurePermissionOfLoggedInUser(ActionRight.SALE);

        CafeteriaUser cafeteriaUser = cafeteriaUserRepository.findByMecanographicNumber(new MecanographicNumber(mecanographicNumber));
        UserCard userCard = cafeteriaUser.userCard();

        CardMovement cardMovement = new CardMovement(MovementType.RECHARGING, Money.euros(money), cafeteriaUser);

        userCard.addMovement(cardMovement);

        return cafeteriaUserRepository.save(cafeteriaUser);
    }
}
