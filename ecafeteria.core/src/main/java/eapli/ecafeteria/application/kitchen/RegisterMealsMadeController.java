package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Calendar;
import java.util.List;

public class RegisterMealsMadeController {

    private MenuRepository menuRepository = PersistenceContext.repositories().menus();

    private MealRepository mealRepository = PersistenceContext.repositories().meals();

    private MealTypeRepository mealTypeRepository = PersistenceContext.repositories().mealTypes();
    
    private DishRepository dishRepository = PersistenceContext.repositories().dishes();

    private Menu menu;

    public RegisterMealsMadeController() {
    }
    
    public Iterable<Dish> allDishes(){
        return dishRepository.findAll();
    }
    
    public Iterable<MealType> getMealTypes(){
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);
        return mealTypeRepository.findAll();
    }

    public Iterable<Meal> getMealsByDate(Calendar date) {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);
        return mealRepository.findByDate(date);
    }

    public Iterable<Meal> getMealsByMealtype(MealType mealType) {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);
        return mealRepository.findByMealType(mealType);
    }

    public Iterable<Meal> getMealsByDish(Dish dish) {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);
        return mealRepository.findByDish(dish);
    }

    public boolean RegisterNumberMealsMade(Meal meal, int mealsMade) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_KITCHEN);
        meal.registerMealsMade(mealsMade);
        this.mealRepository.save(meal);
        return true;
    }
}
