/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.user.Rating;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Mariana Calafate(1150481) e Manuel Gerardo(1150480)
 */
public class ConsultDishRatingsService {
    
    public Map<Integer, Double> showRatingsInformation(Dish dish){
        Map<Integer,Double> map = new HashMap<>(); 
        Iterable<Rating> it = PersistenceContext.repositories().ratings().findAllRatingsByDish(dish);
        int count=0;
        double average=0;
        for(Rating rat : it){
            count++;
            average += rat.stars();
        }
        map.put(count, average/count);
        return map;
    }
    
}
