/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.user.Comment;
import eapli.ecafeteria.domain.user.Rating;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RatingRepository;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Daniel Ribeiro & Miguel Minnemann (1150577 & 1150457)
 */
public class ShowMealsRatingController implements Controller{
    
    private RatingRepository ratingRepository = PersistenceContext.repositories().ratings();
    private ShowMealRatingService svc = new ShowMealRatingService();
    
    public Iterable<Meal> allMealsRated() {
        return svc.allMealsRated();
    }
    
    public void findAllRatingsByMeal() {
        
    }
    
    public void findAllRatingByMeal(Meal meal) {
        svc.findAllRatingByMeal(meal);
    }
    
    public float averageMealRating() {
        return svc.averageMealRating();
    }
    
    public int numberRatingOfMeal() {
        return svc.numberRatingOfMeal();
    }
    
    public Iterable<Comment> mealComments() {
        return svc.mealComments();
    }
    
    public Rating addReplyToComment(final Comment comment, final String reply)
            throws DataIntegrityViolationException, DataConcurrencyException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);

        Rating rating = svc.findRatingWithComment(comment);
        
        rating.addReply(reply);

        return ratingRepository.save(rating);
    }
}
