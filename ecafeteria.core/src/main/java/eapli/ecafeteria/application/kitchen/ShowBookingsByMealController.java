/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.user.Booking;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class ShowBookingsByMealController implements Controller {

    private final MealRepository mealRepository = PersistenceContext.repositories().meals();
    private final BookingRepository bookingRepository = PersistenceContext.repositories().bookings();

    public Iterable<Meal> allMeals() {
        return mealRepository.findAll();
    }

    public Iterable<Booking> findAllBookingsByMeal(final Meal meal) {
        return bookingRepository.findAllBookingsByMeal(meal);
    }
}
