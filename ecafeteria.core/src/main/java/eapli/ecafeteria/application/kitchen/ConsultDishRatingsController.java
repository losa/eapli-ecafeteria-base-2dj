/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.application.meals.ListDishService;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.application.Controller;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Mariana Calafate(1150481) e Manuel Gerardo(1150480)
 */
public class ConsultDishRatingsController implements Controller {
        
    private ListDishService svc = new ListDishService();

    public Iterable<Dish> allDishes() {
        return this.svc.allDishes();
    }
    
    public Map<Integer, Double> showRatingsInformation(Dish dish){
        return new ConsultDishRatingsService().showRatingsInformation(dish);
    }
    
}

