/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.user.Booking;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class ShowBookingsByDishController implements Controller{

    private final DishRepository dishRepository = PersistenceContext.repositories().dishes();
    private final BookingRepository bookingRepository = PersistenceContext.repositories().bookings();
    
    public Iterable<Dish> allDishes() {
        return dishRepository.findAll();
    }
    
    public Iterable<Booking> findAllBookingsByDish(final Dish dish) {
        return bookingRepository.findAllBookingsByDish(dish);
    }
}
