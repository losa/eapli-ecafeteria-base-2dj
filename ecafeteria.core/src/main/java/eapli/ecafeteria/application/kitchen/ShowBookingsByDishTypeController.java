/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.domain.user.Booking;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class ShowBookingsByDishTypeController implements Controller {

    private final DishTypeRepository dishTypeRepository = PersistenceContext.repositories().dishTypes();
    private final BookingRepository bookingRepository = PersistenceContext.repositories().bookings();

    public Iterable<DishType> allDishTypes() {
        return dishTypeRepository.findAll();
    }

    public Iterable<Booking> findAllBookingsByDishType(final DishType dish) {
        return bookingRepository.findAllBookingsByDishType(dish);
    }
}
