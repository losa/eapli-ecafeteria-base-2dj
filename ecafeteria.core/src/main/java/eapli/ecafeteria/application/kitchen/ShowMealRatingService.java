/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.kitchen;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.user.Comment;
import eapli.ecafeteria.domain.user.Rating;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RatingRepository;
import eapli.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Daniel Ribeiro & Miguel Minnemann (1150577 & 1150457)
 */
public class ShowMealRatingService {

    private RatingRepository ratingRepository = PersistenceContext.repositories().ratings();
    private Iterable<Rating> mealRatings;
    
    public Iterable<Meal> allMealsRated() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        
        List<Meal> mealsRated = new LinkedList<>();
        Iterable<Rating> ratings =  ratingRepository.findAll();
        
        for (Rating rating : ratings) {
            mealsRated.add(rating.meal());
        }
        
        return mealsRated;
    }
    
    public void findAllRatingByMeal(Meal meal) {
        if(mealRatings==null) {
            mealRatings=ratingRepository.findAllRatingsByMeal(meal);
        }
    }
    
    public float averageMealRating() {
        int numRatings, sum=0;
        float average=-1;
        
        if(mealRatings!=null) {
            numRatings = Collections.size(mealRatings);
            for (Rating rating : mealRatings) {
                sum+=rating.stars();
            }
            average=sum/numRatings;
        }
        return average;
    }
    
    public int numberRatingOfMeal() {
        return Collections.size(mealRatings);
    }
    
    public Iterable<Comment> mealComments() {
        List<Comment> comments = new LinkedList<>();
        
        for (Rating rating : mealRatings) {
            comments.add(rating.comment());
        }
        return comments;
    }
    
    public Rating findRatingWithComment(Comment comment) {
        for (Rating rating : ratingRepository) {
            if(rating.comment().equals(comment)) {
                return rating;
            }
        }
        return null;
    }
}
