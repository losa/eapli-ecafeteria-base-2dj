/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.UserSession;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.user.Booking;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;

/**
 *
 * @author < José Miguel - 1150641@isep.ipp.pt >
 */
public class DeliveryMealController {

    private final BookingRepository bookingRepository;

    private final POS pos = UserSession.getPOS();

    public DeliveryMealController() {
        bookingRepository = PersistenceContext.repositories().bookings();
    }

    public boolean deliveryMeal(Booking booking) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_DELIVERY);
        booking.deliveryBooking();
        bookingRepository.save(booking);
        return true;
    }

    public Iterable<Booking> getBookingsForToday() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_DELIVERY);
        return bookingRepository.findAllBookingsByDayAndMealType(DateTime.dateToCalendar(pos.DateToWork()), pos.MealToWork());
    }

}
