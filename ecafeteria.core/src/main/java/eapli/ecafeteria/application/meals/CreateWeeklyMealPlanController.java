package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Date;

public class CreateWeeklyMealPlanController implements Controller {
    
    private final ListMealService srv = new ListMealService();
    private final CreateMealPlanService msrv = new CreateMealPlanService();
    private final MealPlanRepository mealPlanRepository = PersistenceContext.repositories().mealPlans();
    private final MealPlanItemRepository mealPlanItemRepository = PersistenceContext.repositories().mealPlanItems();
    
    public Iterable<Meal> mealsInAWeek(Date date) {
        return this.srv.allPublishedMealsInAPeriod(date);
    }
    
    public MealPlanItem registerMealPlanItem(final MealPlan mealPlan, final Meal meal, final int amount) throws DataConcurrencyException, DataIntegrityViolationException {
        MealPlanItem newItem = new MealPlanItem(meal, amount, mealPlan);
        
        return mealPlanItemRepository.save(newItem);
    }
    
    public MealPlan saveMealPlan(String name, Date date) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        
        MealPlan newPlan = msrv.newMealPlan(name, date);
        
        return this.mealPlanRepository.save(newPlan);
    }
    
}
