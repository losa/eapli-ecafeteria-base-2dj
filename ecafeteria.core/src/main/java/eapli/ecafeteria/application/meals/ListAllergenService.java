/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 * @author Luis Oliveira 1150773
 * @author José Silva 1150371
 */
public class ListAllergenService {
    private final AllergenRepository allergenRepository = PersistenceContext.repositories().Allergens();

    public Iterable<Allergen> allAllergens() {
//        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);

        return this.allergenRepository.findAll();
    }

}
