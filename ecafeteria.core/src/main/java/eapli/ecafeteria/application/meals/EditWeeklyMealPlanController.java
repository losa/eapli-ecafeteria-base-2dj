package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author vasco
 */
public class EditWeeklyMealPlanController implements Controller {
    
    private final ListMealPlanService srv = new ListMealPlanService();
    private final ListMealPlanItemService srv1 = new ListMealPlanItemService();
    private final MealPlanRepository mealPlanRepository = PersistenceContext.repositories().mealPlans();
    
    public Iterable<MealPlan> allMealPlans() {
        return this.srv.validMealPlans();
    }
    
    public Iterable<MealPlanItem> mealPlanItemsFromAMealPlan(MealPlan mealPlan) {
        return this.srv1.mealPlanItemsByMealPlan(mealPlan);
    }
    
    public void editMealPlan(MealPlan mealPlan) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        if (mealPlan == null) {
            throw new IllegalArgumentException();
        }

        MealPlan ret = this.mealPlanRepository.save(mealPlan);
    }
    
    public void editMealPlanItem(MealPlanItem mealPlanItem, int newAmount) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        if (mealPlanItem == null) {
            throw new IllegalArgumentException();
        }

        mealPlanItem.changeAmount(newAmount);
    }
}
