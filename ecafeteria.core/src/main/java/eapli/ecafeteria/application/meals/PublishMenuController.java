/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import static eapli.ecafeteria.Application.ensurePermissionOfLoggedInUser;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Nuno Meireles <1150830@isep.ipp.pt>
 * @author José Guimarães <1150812@isep.ipp.pt>
 */
public class PublishMenuController {
    
    public Iterable<Menu> listMenus() throws DataIntegrityViolationException {
         ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        
        final MenuRepository repo = PersistenceContext.repositories().menus();
        
        return repo.unpublishedMenus();
    }
    
    public boolean publishMenu(Menu m) throws DataIntegrityViolationException, DataConcurrencyException {
        
        ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        
        final MenuRepository repo = PersistenceContext.repositories().menus();
        
        if(m.publishMenu()){
            if(repo.save(m)!=null){
                return true;
            }   
        }   
        return false;
    }
}
