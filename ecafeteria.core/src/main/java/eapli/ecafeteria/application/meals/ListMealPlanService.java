/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author vasco
 */
public class ListMealPlanService {
    
    private final MealPlanRepository mealPlanRepository = PersistenceContext.repositories().mealPlans();

    public Iterable<MealPlan> validMealPlans() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);

        return this.mealPlanRepository.findValidMealPlans();
    }
}
