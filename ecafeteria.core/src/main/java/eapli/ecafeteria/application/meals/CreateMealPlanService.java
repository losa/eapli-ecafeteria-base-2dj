package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.Date;

public class CreateMealPlanService {
    
    public CreateMealPlanService() {
        
    }
    
    public MealPlan newMealPlan(String name, Date date) {
        
        int week;
        Calendar c_date, startingDate, endingDate;
        
        c_date = DateTime.dateToCalendar(date);
        
        week = DateTime.weekNumber(c_date);
        
        startingDate = DateTime.beginningOfWeek(DateTime.currentYear(), week);
        
        endingDate = DateTime.endOfWeek(DateTime.currentYear(), week);
        
        if(c_date.equals(DateTime.now())) {
            return new MealPlan(name, c_date, endingDate);
        }
        
        return new MealPlan(name, startingDate, endingDate);
    }
    
}
