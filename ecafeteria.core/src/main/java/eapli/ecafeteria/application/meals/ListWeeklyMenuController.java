/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.application.Controller;

/**
 *
 * @author vasco
 */
public class ListWeeklyMenuController implements Controller {

    private ListMenuService svc = new ListMenuService();

    public Iterable<Menu> allDishes() {
        return this.svc.allDishes();
    }
}
