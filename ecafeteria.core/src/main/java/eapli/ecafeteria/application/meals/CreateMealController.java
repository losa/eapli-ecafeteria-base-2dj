package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Miguel
 */
public class CreateMealController {
    
    private MenuRepository menuRepository = PersistenceContext.repositories().menus();
    
    private ListDishService lds = new ListDishService();
    
    private MealTypeRepository mealTypeRepository = PersistenceContext.repositories().mealTypes();
    
    private MealRepository mealRepository = PersistenceContext.repositories().meals();
    
    private Menu menu;
    
    public CreateMealController(Menu menu){
        this.menu = menu;
    }
    
    public Iterable<Dish> getDishes(){
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        return lds.allDishes();
    }
    
    public Iterable<MealType> getMealTypes(){
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        return mealTypeRepository.findAll();
    }
    
    public boolean createMeal(Calendar date, Dish dish, MealType mealType) throws DataConcurrencyException, DataIntegrityViolationException {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        Meal meal = new Meal(dish, mealType, date, menu);
        mealRepository.save(meal);
        return true;
    }
}
