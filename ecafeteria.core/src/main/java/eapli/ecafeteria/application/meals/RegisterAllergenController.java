/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 * @author Luis Oliveira 1150773
 * @author José Silva 1150371
 */
public class RegisterAllergenController implements Controller {

    private ListAllergenService all = new ListAllergenService();

    private AllergenRepository allergenRepository = PersistenceContext.repositories().Allergens();

    public Allergen registerAllergen(final String name) throws DataIntegrityViolationException, DataConcurrencyException {

        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);

        final Allergen newAllergen = new Allergen(name);

        Allergen ret = this.allergenRepository.save(newAllergen);

        return ret;
    }

    
    public Iterable<Allergen> Allergens() {
        return this.all.allAllergens();
    }
    
    
}
