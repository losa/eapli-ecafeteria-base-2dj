/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author jferr
 */
public class ListMealService {
    private MealRepository mealRepository = PersistenceContext.repositories().meals();

    public Iterable<Meal> allMeals() {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);

        return this.mealRepository.findAll();
    }
    
    public Iterable<Meal> allPublishedMealsInAPeriod(Date date) {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        
        int week;
        Calendar c_dateStart, c_dateEnd, c_date;
        
        c_date = DateTime.dateToCalendar(date);
        
        week = DateTime.weekNumber(c_date);
        
        c_dateStart = DateTime.beginningOfWeek(DateTime.currentYear(), week);
        
        c_dateEnd = DateTime.endOfWeek(DateTime.currentYear(), week);
        
        if(c_date.equals(DateTime.now())) {
            return this.mealRepository.findPublishedMealsInPeriod(c_date, c_dateEnd);
        }
        return this.mealRepository.findPublishedMealsInPeriod(c_dateStart, c_dateEnd);
    }
}
