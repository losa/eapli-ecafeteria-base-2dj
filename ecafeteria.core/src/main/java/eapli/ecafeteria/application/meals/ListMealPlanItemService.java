/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.ecafeteria.persistence.PersistenceContext;

/**
 *
 * @author vasco
 */
public class ListMealPlanItemService {
    
    private final MealPlanItemRepository mealPlanItemRepository = PersistenceContext.repositories().mealPlanItems();

    public Iterable<MealPlanItem> mealPlanItemsByMealPlan(MealPlan mealPlan) {
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);

        return this.mealPlanItemRepository.findMealPlanItemsByMealPlan(mealPlan);
    }
}
