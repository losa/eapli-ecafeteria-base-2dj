package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

public class ListMenuController {
    
    private MenuRepository menuRepository = PersistenceContext.repositories().menus();
    
    public Iterable<Menu> getUnpublishedMenus(){
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        
        return menuRepository.unpublishedMenus();
    }
    
    public List<Menu> getUnpublishedMenusByPeriod(Calendar initialDate, Calendar finalDate){
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        List<Menu> menu = new LinkedList<>();
        for (Menu m: menuRepository.unpublishedMenus()){
            if (m.isInPeriod(initialDate, finalDate)){
                menu.add(m);
            }
        }
        return menu;
    }
    
    public Iterable<Menu> getPublishedMenus(){
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        
        return menuRepository.publishedMenus();
    }
    
}
