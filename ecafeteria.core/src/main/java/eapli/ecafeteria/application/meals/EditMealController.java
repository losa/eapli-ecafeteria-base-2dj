package eapli.ecafeteria.application.meals;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Calendar;

public class EditMealController {
    
    private MenuRepository menuRepository = PersistenceContext.repositories().menus();
    private MealRepository mealRepository = PersistenceContext.repositories().meals();
    private ListDishService svc = new ListDishService();
    private MealTypeRepository mealTypeRepository = PersistenceContext.repositories().mealTypes();
    private Menu menu;
    
    public EditMealController(Menu menu){
        this.menu = menu;
    }
    
    public Iterable<Dish> getDishes(){
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        return svc.allDishes();
    }
    
    public Iterable<MealType> getMealTypes(){
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        return mealTypeRepository.findAll();
    }
    
    public Iterable<Meal> getMeals(){
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        return mealRepository.findByMenu(menu);
    }
    
    public boolean editMeal(Meal meal, Calendar date, Dish dish, MealType mealType) throws DataConcurrencyException, DataIntegrityViolationException{
        Application.ensurePermissionOfLoggedInUser(ActionRight.MANAGE_MENUS);
        meal.editMeal(dish, mealType, date, menu);
        mealRepository.save(meal);
        return true;
    }
}
