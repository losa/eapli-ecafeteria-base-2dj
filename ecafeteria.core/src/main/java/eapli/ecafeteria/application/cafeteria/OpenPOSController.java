/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cafeteria;

import eapli.ecafeteria.AppSession;
import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.UserSession;
import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.POSRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Date;

/**
 *
 * @author Nuno Meireles <1150830@isep.ipp.pt>
 * @author José Guimarães <1150812@isep.ipp.pt>
 */
public class OpenPOSController implements Controller{
    
    RepositoryFactory repoFactory;
    
    POSRepository posRepo;
    
    public OpenPOSController() {
        this.repoFactory = PersistenceContext.repositories();
        
        this.posRepo = repoFactory.pos();
    }
    
    /**
     * Choose POS date and meal to work.
     * 
     * @param dateToWork the date to work.
     * @param type the meal to work.
     * @return true if successful false if otherwise.
     * @throws eapli.framework.persistence.DataConcurrencyException
     * @throws eapli.framework.persistence.DataIntegrityViolationException
     */
    public POS choosePOSToWork(Date dateToWork, MealType type) throws DataConcurrencyException, DataIntegrityViolationException {
         
        UserSession session = Application.session().session();
        POS pos = session.setPos(dateToWork, type);
        this.posRepo.save(pos);

        return pos;
        
    }
    
}
