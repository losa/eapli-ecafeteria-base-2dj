/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.ecafeteria.application.cafeteria;

import eapli.ecafeteria.domain.cafeteria.UserCard;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.domain.Money;
import eapli.util.Collections;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.Iterator;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class CheckWarningsUserBalanceController implements Controller {
    
    private MealRepository mealRepository = PersistenceContext.repositories().meals();
    
    public boolean checkIfUserBalanceNeedsWarning(UserCard userCard) {
        final Money balance = userCard.balance();
        double sumAux = 0, averageMealPrice;
        int numberOfMeals;
        
        
        DateTime.beginningOfWeek(0, 0);
        
        Calendar beginDate = DateTime.beginningOfWeek(DateTime.currentYear(), DateTime.currentWeekNumber());
        Calendar endDate = DateTime.endOfWeek(DateTime.currentYear(), DateTime.currentWeekNumber());
        
        Iterable<Meal> mealsInPeriod = mealRepository.findByPeriod(beginDate, endDate);
        
        for (Meal meal : mealsInPeriod) {
            sumAux += meal.dishFromMeal().currentPrice().amount();
        }
        
        numberOfMeals = Collections.size(mealsInPeriod);
        
        averageMealPrice = sumAux/numberOfMeals;
        
        return balance.amount() < averageMealPrice*5;
    }

}
