/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cafeteria;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.UserAllergen;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.UserAllergenRepository;

/**
 *
 * @author Rui Peixoto
 */
public class ListUserAllergensService {

    private final UserAllergenRepository userAllergenRepository = PersistenceContext.repositories().userAllergens();
    private final CafeteriaUserRepository cafeteriaUserRepository = PersistenceContext.repositories().cafeteriaUsers(null);

    public Iterable<UserAllergen> getUserAllergens() {
        CafeteriaUser cafeteriaUser = cafeteriaUserRepository.findByUsername(Application.session().session().authenticatedUser().username());
        return userAllergenRepository.findAllergenByCafeteriaUser(cafeteriaUser);
    }
}
