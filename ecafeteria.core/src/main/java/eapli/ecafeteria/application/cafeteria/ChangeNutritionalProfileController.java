/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cafeteria;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.NutritionalProfile;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Rui Peixoto
 */
public class ChangeNutritionalProfileController implements Controller {

    private final CafeteriaUserRepository repository = PersistenceContext.repositories().cafeteriaUsers(null);

    public NutritionalProfile getActualNutritionalProfile() {

        CafeteriaUser cafeteriaUser = repository.findByUsername(Application.session().session().authenticatedUser().username());

        return cafeteriaUser.nutritionalProfile();
    }

    public CafeteriaUser changeNutritionalProfile(int maxCaloriesPerMeal, int maxCaloriesPerWeek, int maxSaltPerMeal, int maxSaltPerWeek) throws DataConcurrencyException, DataIntegrityViolationException {

        CafeteriaUser cafeteriaUser = repository.findByUsername(Application.session().session().authenticatedUser().username());

        cafeteriaUser.changeNutritionalProfile(maxCaloriesPerMeal, maxSaltPerMeal, maxCaloriesPerWeek, maxSaltPerWeek);

        return repository.save(cafeteriaUser);
    }

}
