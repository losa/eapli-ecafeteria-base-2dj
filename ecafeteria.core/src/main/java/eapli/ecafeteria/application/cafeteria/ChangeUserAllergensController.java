/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cafeteria;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.UserAllergen;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.UserAllergenRepository;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;

/**
 *
 * @author Rui Peixoto
 */
public class ChangeUserAllergensController implements Controller {

    private final UserAllergenRepository userAllergenRepository = PersistenceContext.repositories().userAllergens();
    private final CafeteriaUserRepository cafeteriaUserRepository = PersistenceContext.repositories().cafeteriaUsers(null);


    public Iterable<UserAllergen> getUserAllergens() {
        CafeteriaUser cafeUser = cafeteriaUserRepository.findByUsername(Application.session().session().authenticatedUser().username());
        return userAllergenRepository.findAllergenByCafeteriaUser(cafeUser);
    }

    public UserAllergen addNewAllergen(Allergen allergen) throws DataConcurrencyException, DataIntegrityViolationException {
        CafeteriaUser cafeUser = cafeteriaUserRepository.findByUsername(Application.session().session().authenticatedUser().username());
       final UserAllergen userAllergen = new UserAllergen(cafeUser, allergen);
        for(UserAllergen userAll : userAllergenRepository.findAllergenByCafeteriaUser(cafeUser)){
            if (userAll.equals(userAllergen)){
                return null;
            }
        }      
        return userAllergenRepository.save(userAllergen);
        
    }

    public void deleteAllergen(UserAllergen userAllergen) throws DataConcurrencyException, DataIntegrityViolationException {
        userAllergenRepository.delete(userAllergen);
    }

    public UserAllergen addNewAllergenForBoostrap(CafeteriaUser cafeteriaUserBoostrap, Allergen allergen) throws DataConcurrencyException, DataIntegrityViolationException {
        CafeteriaUser cafeUser = cafeteriaUserRepository.findByUsername(cafeteriaUserBoostrap.user().username());
        final UserAllergen userAllergen = new UserAllergen(cafeUser, allergen);
        return userAllergenRepository.save(userAllergen);
    }
}
