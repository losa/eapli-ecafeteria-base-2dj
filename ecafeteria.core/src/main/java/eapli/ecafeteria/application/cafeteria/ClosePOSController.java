/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.application.cafeteria;

import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.domain.authz.UserSession;
import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.user.Booking;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author 1150831
 */
public class ClosePOSController {

    private RepositoryFactory factory;
    private POS pos;
    private BookingRepository bookingRepository;
    private MealRepository mealRepository;
    private UserSession session;

    public ClosePOSController() {

        session = AppSettings.instance().session();
        factory = PersistenceContext.repositories();
        bookingRepository = factory.bookings();
        mealRepository = factory.meals();
        pos = session.getPOS();
    }

    public ArrayList UndeliveredMeals() {
        Iterable allMeals = mealRepository.findAll();
        Meal meal = null;
        try{
        for (Iterator iterator = allMeals.iterator(); iterator.hasNext();) {
            Meal next = (Meal) iterator.next();
            if (next.typeOfMeal().equals(pos.MealToWork()) && next.date().equals(DateTime.dateToCalendar(pos.DateToWork()))) {
                meal = next;
            }

        }
        }catch(NullPointerException e){
            System.err.println("There is no meal ");
       }
        
        Iterable undeliveredMeals = bookingRepository.findAllBookingsByMeal(meal);
        ArrayList<Booking> delivered = new ArrayList<>();
        for (Iterator iterator = undeliveredMeals.iterator(); iterator.hasNext();) {
            Booking next = (Booking) iterator.next();
            if (!next.bookingUndelivered()) {
                delivered.add(next);
            }

        }

        pos.closePOS();
        return delivered;
    }

}
