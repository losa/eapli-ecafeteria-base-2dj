package eapli.ecafeteria.domain.kitchen;

import java.util.Objects;

public class RawMaterial {
    
    private String rmCode;
    private Material material;
    private Lot lot;

    
    /*for ORM*/
    protected RawMaterial() {
        
    }
     
    
    public RawMaterial(Material material, Lot lot) { 
        
        if (material == null) {
            throw new IllegalStateException("The material is invalid!");
        }
        
        if (lot == null) {
            throw new IllegalStateException("The lot is invalid!");
        }
            
        this.material = material;       
        this.lot = lot;      
        this.rmCode = material.id() + "_" + lot.getLotCode();
    }

    
    /*GETTERS*/
    public String getRmCode() {
        return rmCode;
    }

//    public Material getMaterial() {
//        return material;
//    }
//
//    public Lot getLot() {
//        return lot;
//    }

    
    
    public boolean is(String id) {
        return rmCode.equalsIgnoreCase(this.getRmCode());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RawMaterial)) {
            return false;
        }

        final RawMaterial other = (RawMaterial) o;
        return getRmCode().equals(other.getRmCode());
    }
    
   
    
    @Override
    public int hashCode() {
        return this.rmCode.hashCode();
    }
    
    
    
    
    
    
    
}
