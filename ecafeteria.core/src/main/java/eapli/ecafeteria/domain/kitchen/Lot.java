package eapli.ecafeteria.domain.kitchen;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;


public class Lot {
    
 
    private String lotCode;
    private Date arrivalDate;  
        
    
    /*for ORM*/
    protected Lot() {
        
    }

    public Lot(String lotCode, Date arrivalDate, int availableDoses) {
          
        if( arrivalDate.after(Calendar.getInstance().getTime()) || arrivalDate == null ) {
            arrivalDate = Calendar.getInstance().getTime();
        } else {
            this.arrivalDate = arrivalDate;
        }         
        this.lotCode = generateLotCode(this.arrivalDate);
         
    }
    
    
    /*GETTERS*/
    public String getLotCode() {
        return this.lotCode;
    }

//    public Date getArrivalDate() {
//        return arrivalDate;
//    }
//
//    
//     
      /*SETTERS*/
//    public void setLotCode(String lotCode) {
//        this.lotCode = lotCode;
//    }
//
//    public void setArrivalDate(Date arrivalDate) {
//        this.arrivalDate = arrivalDate;
//    }


    public boolean is(String id) {
        return id.equalsIgnoreCase(this.getLotCode());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Lot)) {
            return false;
        }

        final Lot other = (Lot) o;
        return getLotCode().equals(other.getLotCode());
    }
    
   
    
    @Override
    public int hashCode() {
        return this.lotCode.hashCode();
    }

    
 
    /*GENERATE LOT CODE IN CONSTRUCTOR*/
    private String generateLotCode (Date d) {      
        String code;
        code = "LOT_" + d.toString();            
        return code;
    }

    
    
 
    
    
}
