/**
 *
 */
package eapli.ecafeteria.domain.authz;

import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.domain.ValueObject;
import java.util.Date;
import java.util.UUID;

/**
 * @author Paulo Gandra Sousa
 *
 */
public class UserSession implements ValueObject {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final SystemUser user;
    private final UUID token;
    private static POS pos;

    public UserSession(SystemUser user) {
        if (user == null) {
            throw new IllegalStateException("user must not be null");
        }
        this.user = user;
        this.token = UUID.randomUUID();
    }

    public SystemUser authenticatedUser() {
        return this.user;
    }
    
    // Returns the open POS
    public static POS getPOS(){
        return UserSession.pos;
    }
    
    // Opens the POS relative to a date and mealtype
    public POS setPos(Date dateToWork, MealType type) {
        this.pos = new POS(dateToWork, type);
        return this.pos;
    }
    
    @Override
    public String toString() {
        return this.user.id() + "@" + this.token;
    }
}
