/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.user;

import eapli.util.Strings;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 *
 * @author Miguel Minnemann (1150457) & Daniel Ribeiro (1150577)
 */
@Entity
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long pk;
    @Version
    private Long version;
    
    private String comment;
    private Comment reply;
    
    protected Comment() {
        //ORM 
    }
    
    public Comment(String comment) {
        setComment(comment);
    }
    
    private void setComment(String comment) {
        if (!Strings.isNullOrEmpty(comment)) {
            this.comment = comment;
        } else {
            throw new IllegalArgumentException("Invalid Comment");
        }
    }
    
    private void setReply(Comment reply) {
        if(reply != null) {
            this.reply = reply;
        } else {
            throw new IllegalArgumentException("Invalid Reply");
        }
    }
    
    public String comment() {
        return comment;
    }
    public void addReply (Comment reply) {
       setReply(reply);
    }
    
    @Override
    public int hashCode() {
        return pk.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Comment other = (Comment) obj;
        return Objects.equals(this.pk, other.pk);
    }
}
