/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.user;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.util.DateTime;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
@Entity
public class Booking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long pk;
    @Version
    private Long version;

    private Meal meal;
    private CafeteriaUser user;
    private Calendar date;
    private BookingState state;

    protected Booking() {
        //ORM 
    }

    public Booking(Meal meal, CafeteriaUser user) {
        setMeal(meal);
        setUser(user);
        date = DateTime.now();
        state = BookingState.EffectiveBooking;
    }

    private void setMeal(Meal meal) {
        if (meal != null) {
            this.meal = meal;
        } else {
            throw new IllegalArgumentException("Invalid Meal");
        }
    }

    private void setUser(CafeteriaUser user) {
        if (user != null) {
            this.user = user;
        } else {
            throw new IllegalArgumentException("Invalid User");
        }
    }

    public Calendar bookingDate() {
        return date;
    }

    public Meal meal() {
        return meal;
    }

    public CafeteriaUser user() {
        return user;
    }

    public boolean cancelBooking() {
        if (state == BookingState.EffectiveBooking) {
            state = BookingState.Cancelled;
            return true;
        }
        return false;
    }

    public boolean deliveryBooking() {
        if (state == BookingState.EffectiveBooking) {
            state = BookingState.Delivered;
            return true;
        }
        return false;
    }

    public boolean bookingUndelivered() {
        if (state == BookingState.EffectiveBooking) {
            state = BookingState.UnDelivered;
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return date.hashCode(); //FIXME we should not this
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Booking other = (Booking) obj;
        return Objects.equals(this.pk, other.pk);
    }
}
