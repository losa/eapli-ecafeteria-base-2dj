/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.user;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public enum BookingState {
    
    EffectiveBooking,
    Cancelled,
    Delivered,
    UnDelivered
    
}
