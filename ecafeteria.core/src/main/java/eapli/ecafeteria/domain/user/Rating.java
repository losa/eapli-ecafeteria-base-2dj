/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.user;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.util.DateTime;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 *
 * @author Miguel Minnemann (1150457) & Daniel Ribeiro (1150577)
 */
@Entity
public class Rating implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long pk;
    @Version
    private Long version;
    
    private int starNumber;
    private SystemUser user;
    private Meal meal;
    private Comment comment;
    private Calendar date;
    
    protected Rating() {
        //ORM 
    }
    
    public Rating(int starNumber, Meal meal, SystemUser user) {
        setStarNumber(starNumber);
        setMeal(meal);
        setUser(user);
        date = DateTime.now();
    }
    
    public Rating(int starNumber, Meal meal, SystemUser user, Comment comment) {
        setStarNumber(starNumber);
        setMeal(meal);
        setUser(user);
        setComment(comment);
        date = DateTime.now();
    }
    
    private void setStarNumber(int starNumber) {
        if(starNumberRequirements(starNumber)) {
            this.starNumber = starNumber;
        } else {
            throw new IllegalArgumentException("Invalid Star Number");
        }
    }
    
    private boolean starNumberRequirements(int starNumber) {
        return starNumber <= 5 && starNumber >= 1;
    }
    
    public SystemUser user() {
        return user;
    }
    
    public Meal meal() {
        return meal;
    }
    
    public int stars() {
        return starNumber;
    }
    
    public Comment comment() {
        return comment;
    }
    
    private void setMeal(Meal meal) {
        if(meal != null) {
            this.meal = meal;
        } else {
            throw new IllegalArgumentException("Invalid Meal");
        }
    }
    
    private void setUser(SystemUser user) {
         if (user != null) {
            this.user = user;
        } else {
            throw new IllegalArgumentException("Invalid User");
        }
    }
    
    private void setComment(Comment comment) {
        if(comment != null) {
            this.comment = comment;
        } else {
            throw new IllegalArgumentException("Invalid Comment");
        }
    }
    
    public void addComment(Comment comment) {
       setComment(comment);
    }
    
    public void addReply(String reply) {
        comment.addReply(new Comment(reply));
    }

    @Override
    public int hashCode() {
        return pk.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rating other = (Rating) obj;
        return Objects.equals(this.pk, other.pk);
    }
}
