/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.complaints;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Dish;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 *
 * @author gustavo (1140320), Vasco (1140338)
 */
@Entity
public class Complaint implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long pk;
    
    @Version
    private Long version;
    
    
    private String complaint;
    private Dish dish;
    private CafeteriaUser user;
    
    public Complaint(final String complaint, Dish dish, CafeteriaUser user) {
        if (complaint == null) {
            throw new IllegalStateException();
        }
        this.complaint = complaint;
        this.dish = dish;
        this.user = user;
    }

    protected Complaint() {
    }

    private void setDish(Dish dish) {
        if (dish != null) {
            this.dish = dish;
        } else {
            throw new IllegalArgumentException("Invalid dish");
        }
    }
    
    public String complaint() {
        return this.complaint;
    }
    
    public Dish dish(){
        return dish;
    }
    
    public CafeteriaUser user() {
        return user;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Complaint)) {
            return false;
        }

        final Complaint other = (Complaint) o;
        return complaint().equals(other.complaint()) && dish().equals(other.dish()) &&
                user().equals(other.user());
    }

    @Override
    public int hashCode() {
        return complaint.hashCode();
    }
}
