/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.ddd.AggregateRoot;
import eapli.framework.domain.Designation;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EmbeddedId;
import javax.persistence.Id;

/**
 * @author Luis Oliveira 1150773
 * @author José Silva 1150371
 */

@Entity
public class Allergen implements AggregateRoot<String>, Serializable {

    @Id
    private String name;
    
    private boolean active;

    public Allergen(final String name) {
        if (name == null) {
            throw new IllegalStateException();
        }
        this.name = name;
        this.active = true;
    }

    protected Allergen() {
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Allergen)) {
            return false;
        }

        final Allergen other = (Allergen) o;
        return id().equals(other.id());
    }

    @Override
    public boolean sameAs(Object other) {
        if (!(other instanceof Allergen)) {
            return false;
        }

        final Allergen that = (Allergen) other;
        if (this == that) {
            return true;
        }

        return id().equals(that.id());
    }

    @Override
    public boolean is(String id) {
        return id().equals(id);
    }
    
    public boolean isActive(){
        return this.active;
    }

    @Override
    public String id() {
        return this.name;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
    
    public String name() {
        return this.name;
    }

    
   
    
}
