/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.domain.meals;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import eapli.framework.domain.Money;
import eapli.framework.domain.ddd.ValueObject;
import eapli.framework.domain.range.TimePeriod;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 */
@Entity
public class PriceOnPeriod implements ValueObject, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long pk;

    private Money price;
    private TimePeriod validity;

    protected PriceOnPeriod() {
	// for orm
    }

    private PriceOnPeriod(Money price) {
	if (price == null || price.amount() <= 0) {
	    throw new IllegalStateException();
	}
	this.price = price;
    }

    public PriceOnPeriod(Calendar startsOn, Calendar finishesOn, Money price) {
	this(price);
	validity = new TimePeriod(startsOn, finishesOn);
    }

    public PriceOnPeriod(Calendar startsOn, Money price) {
	this(price);
	validity = new TimePeriod(startsOn);
    }

    public Money price() {
	return price;
    }

    public boolean includes(Calendar onDate) {
	return validity.includes(onDate);
    }

    public Calendar startsOn() {
	return validity.start();
    }
}
