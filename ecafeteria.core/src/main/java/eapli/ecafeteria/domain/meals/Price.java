/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.domain.meals;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import eapli.framework.domain.Money;
import eapli.util.DateTime;

/**
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 */
@Entity
public class Price implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue // (strategy = GenerationType.AUTO)
    private Long pk;

    @OneToMany(cascade = CascadeType.ALL)
    private final List<PriceOnPeriod> prices = new ArrayList<>();

    public Price(Calendar startsOn, Money price) {
	prices.add(new PriceOnPeriod(startsOn, price));
    }

    public Price(Money price) {
	this(DateTime.now(), price);
    }

    protected Price() {
	// for orm
    }

    void update(Calendar startsOn, Money price) {
	if (!prices.isEmpty()) {
	    // remove the current price (position = 0)
	    final PriceOnPeriod lastPrice = prices.remove(0);

	    // close the period of validity and add element
	    prices.add(0, new PriceOnPeriod(lastPrice.startsOn(), startsOn, lastPrice.price()));
	}

	// add the new price with an open interval of validity
	prices.add(0, new PriceOnPeriod(startsOn, price));
    }

    public Money current() {
	if (prices.isEmpty()) {
	    throw new IllegalStateException();
	}

	return prices.get(0).price();
    }

    public Money onDate(Calendar date) {
	if (prices.isEmpty()) {
	    throw new IllegalStateException();
	}

	final Iterator<PriceOnPeriod> iter = prices.iterator();
	while (iter.hasNext()) {
	    final PriceOnPeriod elem = iter.next();
	    if (elem.includes(date)) {
		return elem.price();
	    }
	}

	return null;
    }
}
