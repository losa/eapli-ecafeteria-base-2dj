/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.ecafeteria.domain.cafeteria.UserAllergen;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Embeddable;

/**
 *
 * @author 1150831
 */
@Embeddable
public class DishAllergen implements Serializable {
    
    private Dish dish;
    private Allergen allergen;
    
    public DishAllergen() {
        //for ORM
    }
    
    public DishAllergen(Dish dish, Allergen allergen) {
        this.dish = dish;
        this.allergen = allergen;
    }
    
    public Dish dish() {
        return dish;
    }
    
    public Allergen allergen() {
        return allergen;
    }
    
    @Override
    public int hashCode() {
        return dish.hashCode() + allergen.hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DishAllergen)) {
            return false;
        }
        
        final DishAllergen other = (DishAllergen) obj;
        
        return (allergen().equals(other.allergen()) && dish().equals(other.dish()));
    }

    public Map<Allergen, Boolean> checkUserAllergen(UserAllergen userAllergens) {
        Map<Allergen,Boolean> allergensInCommon = new HashMap<>();
        /*for(Allergen a : this.allergens())
            allergensInCommon.put(a, userAllergens.containsAllergen(a));*/
        return allergensInCommon;
    }
    
}
