/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.Designation;
import eapli.util.DateTime;
import java.util.Objects;
import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import eapli.framework.domain.ddd.AggregateRoot;
/**
 *
 * @author Miguel Minnemann (1150457) & Daniel Ribeiro
 */

/**
 * A meal. an item of a menu.
 *
 * TODO implement rest of the class
 *
 * @author Paulo Gandra Sousa
 *
 */
@Entity
public class Meal implements AggregateRoot<Long>, Serializable {

    private static final long serialVersionUID = 1L;

    public enum MealStatus {
	WIP, MADE
    }
    
    @Id
    @GeneratedValue
    private Long pk;
    @Version
    private Long version;
    Dish dish;
    
    MealType mealType;
    @Temporal(TemporalType.DATE)
    Calendar ofDay;
    @ManyToOne(cascade = CascadeType.MERGE)
    Menu menu;
    
    private int numberMealsMade;
    
    MealStatus status;

    protected Meal() {
        //ORM products
    }

    public Meal(Dish dish, MealType mealType, Calendar date, Menu menu) {
        setDate(date);
        setDish(dish);
        setMealType(mealType);
        setMenu(menu);
        this.numberMealsMade = 0;
        this.status = MealStatus.WIP;
    }
    
    public void editMeal(Dish dish, MealType mealType, Calendar date, Menu menu){
        setDate(date);
        setDish(dish);
        setMealType(mealType);
        setMenu(menu);
    }
    
    public boolean registerMealsMade(int numberMealsMade){
        if (this.status== MealStatus.WIP && menu.isPublished()) {
            this.numberMealsMade = numberMealsMade;
            return true;
        }
        return false;
    }
    
    public boolean dateIsInPeriod(Calendar initialDate, Calendar finalDate){
        return DateTime.isInPeriod(ofDay, initialDate, finalDate);
    }
    
    public Menu menu() {
	return this.menu;
    }
    
    public Calendar date(){
        return this.ofDay;
    }
    
    public Designation mealType(){
        return this.mealType.name();
    }
    
    public MealType typeOfMeal(){
        return this.mealType;
    }
    
    
    public Designation dishName(){
        return this.dish.name();
    }
    
    public Dish dishFromMeal(){
        return dish;
    }

    private void setDish(Dish dish) {
        if (dish != null) {
            this.dish = dish;
        } else {
            throw new IllegalArgumentException("Invalid dish");
        }
    }
    
    private void setMenu(Menu menu){
        if (menu != null) {
            this.menu = menu;
        } else {
            throw new IllegalArgumentException("Invalid menu");
        }
    }

    private void setMealType(MealType mealType) {
        if (mealType != null) {
            this.mealType = mealType;
        } else {
            throw new IllegalArgumentException("Invalid Meal Type");
        }
    }

    private void setDate(Calendar date) {
        if (date.before(DateTime.beginningOfWeek(DateTime.currentYear(), DateTime.currentWeekNumber()))) { //FIXME temporary fix
            throw new IllegalArgumentException("Invalid Date");
        } else {
            this.ofDay = date;
        }
    }

    @Override
    public int hashCode() {
        return pk.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Meal other = (Meal) obj;
        return Objects.equals(this.pk, other.pk);
    }

    
    @Override
    public Long id() {
	return this.pk;
    }

    @Override
    public boolean is(Long id) {
	return id().equals(id);
    }

    @Override
    public boolean sameAs(Object other) {
	if (!(other instanceof Meal)) {
	    return false;
	}

	final Meal that = (Meal) other;
	if (this == that) {
	    return true;
	}

	return id().equals(that.id());
    }

    
}
