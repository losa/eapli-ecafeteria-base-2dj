/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.Designation;
import eapli.util.Strings;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 *
 * @author Miguel Minnemann (1150457) & Daniel Ribeiro (1150577)
 */
@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"designation"})})
public class MealType implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long pk;
    @Version
    private Long version;

    private Designation name;
    
    protected MealType() {
        //ORM
    }
    
    public MealType(String name) {
        setName(name);
    }
    
    private void setName(String name) {
        if (!Strings.isNullOrEmpty(name)) {
            this.name = Designation.valueOf(name);
        } else {
            throw new IllegalArgumentException("Invalid Name");
        }
    }
    
    public Designation name(){
        return this.name;
    }
    
    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MealType other = (MealType) obj;
        return Objects.equals(this.pk, other.pk);
    }

}


