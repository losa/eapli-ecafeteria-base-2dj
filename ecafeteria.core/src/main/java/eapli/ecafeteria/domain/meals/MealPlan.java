/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.domain.meals;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

import eapli.framework.domain.Designation;
import eapli.framework.domain.ddd.AggregateRoot;
import eapli.framework.domain.range.TimePeriod;
import eapli.util.Strings;

/**
 * A Meal Plan is a view over a set of Meal Plan Items between two dates and it
 * is defined by two dates and a name
 *
 * Each Item is a meal proposed by a specific menu for a certain period of time
 * e.g., a week or a day, added with a set of quantities like: planned meals;
 * cooked meals;
 *
 * There are no explicit references between MealPlan and MealPlanItem
 *
 * Different plans can overlap their time periods meaning that a mealPlanItem
 * can appear in multiple plans For instance one working the in kitchen can use
 * a two day plan and other person working in provisioning use a two week plan
 *
 * @author ajs
 */
@Entity
public class MealPlan implements AggregateRoot<Designation>, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long pk;

    @Version
    private Long version;

    // The name of the plan can be proposed automagically
    // for instance Plan of Week XX
    private Designation name;
    @Embedded
    private TimePeriod timePeriod;

    protected MealPlan() {
	// for ORM
    }

    public MealPlan(final String name, final Calendar beginDate, final Calendar endDate) {
	if (beginDate == null || endDate == null || Strings.isNullOrEmpty(name)) {
	    throw new IllegalStateException();
	}

	this.name = Designation.valueOf(name);
	timePeriod = new TimePeriod(beginDate, endDate);
    }

    @Override
    public boolean equals(Object o) {
	if (this == o) {
	    return true;
	}
	if (!(o instanceof MealPlan)) {
	    return false;
	}

	final MealPlan other = (MealPlan) o;
	return id().equals(other.id());
    }

    @Override
    public int hashCode() {
	return id().hashCode();
    }

    @Override
    public Designation id() {
	return name;
    }

    @Override
    public boolean is(Designation id) {
	return id().equals(id);
    }

    @Override
    public boolean sameAs(Object other) {
	if (!(other instanceof MealPlan)) {
	    return false;
	}

	final MealPlan that = (MealPlan) other;
	if (this == that) {
	    return true;
	}

	return id().equals(that.id());
    }

    public Designation name() {
	return name;
    }

    public Calendar beginDate() {
	return timePeriod.start();
    }

    public Calendar endDate() {
	return timePeriod.end();
    }

    public void cancelBooking(Meal meal) {
	throw new UnsupportedOperationException("Not implemented yet!");
    }

    public void registerBooking(Meal meal) {
	throw new UnsupportedOperationException("Not implemented yet!");
    }

    public void deliverMeal(Meal meal) {
	throw new UnsupportedOperationException("Not implemented yet!");
    }

    public void registerNotDeliveredMeals(Meal meal) {
	throw new UnsupportedOperationException("Not implemented yet!");
    }
}
