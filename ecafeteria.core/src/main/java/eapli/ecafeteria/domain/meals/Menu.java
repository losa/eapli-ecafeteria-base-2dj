package eapli.ecafeteria.domain.meals;

import java.util.Objects;
import java.io.Serializable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

import eapli.framework.domain.TimePeriod2;
import eapli.framework.domain.ddd.AggregateRoot;
import java.util.Calendar;

@Entity
public class Menu implements AggregateRoot<Long>, Serializable {

    public enum MenuStatus {
	WIP, PUBLISHED, ABANDONED
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long pk;
    @Version
    private Long version;  
    String description;

    @Embedded
    TimePeriod2 timePeriod;

    MenuStatus status;
    
    protected Menu(){
        // for ORM
    }
    
    public Menu(String description, TimePeriod2 timePeriod){
        setDescription(description);
        setTimePeriod(timePeriod);
        this.status = MenuStatus.WIP;
    }

    public boolean isInPeriod(Calendar initialDate, Calendar finalDate){
        return this.timePeriod.isInPeriod(initialDate, finalDate);
    }
    
    public String description() {
	return description;
    }
    
    public TimePeriod2 timePeriod() {
	return timePeriod;
    }
    
    private void setDescription(String description){
        if (description == null || description.isEmpty()){
            throw new IllegalArgumentException("Description can't be empty!");
        }else{
            this.description = description;
        }
    }
    
    private void setTimePeriod(TimePeriod2 timePeriod){
        if (timePeriod == null){
            throw new IllegalArgumentException("Time Period can't be null!");
        }else if (!timePeriod.checkPeriod()){
            throw new IllegalArgumentException("Intial date can't be after final date!");
        }else{
            this.timePeriod = timePeriod;
        }
    }
    
    public boolean publishMenu(){
        this.status = MenuStatus.PUBLISHED;
        return true;
    }
    
    public boolean isPublished(){
        return this.status == MenuStatus.PUBLISHED;
    }
    
    @Override
    public int hashCode() {
        return pk.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Menu other = (Menu) obj;
        return Objects.equals(this.pk, other.pk);
    }

    @Override
    public Long id() {
	return pk;
    }

    @Override
    public boolean is(Long id) {
	return id().equals(id);
    }

    @Override
    public boolean sameAs(Object other) {
	if (!(other instanceof Menu)) {
	    return false;
	}

	final Menu that = (Menu) other;
	if (this == that) {
	    return true;
	}

	return id().equals(that.id());
    }

}
