package eapli.ecafeteria.domain.meals;

import eapli.framework.domain.ddd.AggregateRoot;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity
public class MealPlanItem implements Serializable, AggregateRoot<Long> {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue
    private Long pk;
    @Version
    private Long version;
    
    private Meal meal;
    private int amount;
    
    @ManyToOne(cascade = CascadeType.MERGE)
    private MealPlan mealPlan;
    
    protected MealPlanItem() {
        //ORM
    }
    
    public MealPlanItem(Meal meal, int amount, MealPlan plan) {
        setMeal(meal);
        setAmount(amount);
        setMealPlan(plan);
    }
    
    private void setMeal(Meal meal) {
        if(meal == null) {
            throw new IllegalArgumentException("Invalid meal!");
        }
        this.meal = meal;
    }
    
    private void setAmount(int amount) {
        if(amount <= 0) {
            throw new IllegalArgumentException("Amount must be higher than zero!");
        }
        this.amount = amount;
    }
    
    private void setMealPlan(MealPlan plan) {
        if(plan == null) {
            throw new IllegalArgumentException("Invalid meal plan!");
        }
        this.mealPlan = plan;
    }
    
    public Meal meal() {
        return meal;
    }
    
    public int amount() {
        return amount;
    }
    
    public MealPlan mealPlan() {
        return mealPlan;
    }
    
    public void changeAmount(int newAmount) {
        if(newAmount <= 0) {
            throw new IllegalArgumentException("Amount must be higher than zero!");
        }
        this.amount = newAmount;
    }
    
    @Override
    public boolean equals(Object o) {
        if(o == null) return false;
        
        if(!(o instanceof MealPlanItem)) return false;
        
        MealPlanItem other = (MealPlanItem) o;
        
        if(!other.meal().equals(this.meal)) return false;
        
        if(!other.mealPlan().equals(this.mealPlan)) return false;
        
        return other.amount() == this.amount;
    }

    @Override
    public int hashCode() {
        int hash;
        hash = Objects.hashCode(this.meal);
        hash += this.amount;
        hash += Objects.hashCode(this.mealPlan);
        return hash;
    }
    
    @Override
    public Long id() {
	return this.pk;
    }

    @Override
    public boolean is(Long id) {
	return id().equals(id);
    }

    @Override
    public boolean sameAs(Object other) {
        
        if(other == null) return false;
        
	if (!(other instanceof MealPlanItem)) {
	    return false;
	}

	final MealPlanItem that = (MealPlanItem) other;
	if (this == that) {
	    return true;
	}

	return id().equals(that.id());
    }
    
}
