/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import eapli.framework.domain.Money;
import eapli.framework.domain.ddd.AggregateRoot;

/**
 *
 * @author mcn
 */
@Entity
public class CardMovement implements AggregateRoot<Long>, Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private MovementType type;
    private Money ammount;
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    private CafeteriaUser user;
    @Temporal(TemporalType.DATE)
    private Calendar occurredOn;

    protected CardMovement() {
	// for ORM tool only
    }

    public CardMovement(MovementType type, Money ammount, CafeteriaUser userCard) {
	this(type, ammount, userCard, Calendar.getInstance());
    }

    public CardMovement(MovementType type, Money ammount, CafeteriaUser user, Calendar dateOccurrence) {
	if (type == null || ammount == null || user == null || dateOccurrence == null) {
	    throw new IllegalArgumentException();
	}
	this.type = type;
	this.ammount = ammount;
	this.user = user;
	occurredOn = dateOccurrence;
    }

    public UserCard account() {
	return null; //user.card(); dá erro para compilar JAL
    }

    public MovementType movementType() {
	return type;
    }

    public Calendar occurredOn() {
	return occurredOn;
    }

    public Money ammount() {
	return ammount;
    }

    public Money accountingValue() {
	Money result = ammount;
	if (type == MovementType.PURCHASE) {
	    result = result.negate();
	}
	return result;
    }

    @Override
    public boolean sameAs(Object other) {
	if (!(other instanceof CardMovement)) {
	    return false;
	}

	final CardMovement that = (CardMovement) other;
	if (this == that) {
	    return true;
	}
	return id().equals(that.id());
    }

    @Override
    public boolean is(Long id) {
	return id().equals(id);
    }

    @Override
    public Long id() {
	return id;
    }
}
