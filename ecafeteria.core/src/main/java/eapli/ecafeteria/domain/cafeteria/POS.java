/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import eapli.ecafeteria.domain.meals.MealType;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Nuno Meireles <1150830@isep.ipp.pt>
 * @author José Guimarães <1150812@isep.ipp.pt>
 */
@Entity
public class POS implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // Date to work
    @Temporal(TemporalType.DATE)
    private Date dateToWork;

    // (Lunch or Dinner)
    private MealType mealToWork;

    @Enumerated(EnumType.STRING)
    private POSState state;

    /**
     * POS construtor
     */
    protected POS() {
    }

    /**
     * Complete POS construtor
     */
    public POS(Date dateToWork, MealType mealToWork) {
        this.dateToWork = dateToWork;
        this.mealToWork = mealToWork;
        this.state = POSState.OPEN;
    }
    
    public Date DateToWork() {
        return this.dateToWork;
    }

    public MealType MealToWork() {
        return this.mealToWork;
    }

    public void setState(POSState state) {
        this.state = state;
    }

    public boolean POSActive() {
        return this.state == POSState.OPEN;
    }
    public void closePOS() {
        this.state = POSState.CLOSED;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final POS other = (POS) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.dateToWork, other.dateToWork)) {
            return false;
        }
        if (this.mealToWork != other.mealToWork) {
            return false;
        }
        return true;
    }
}
