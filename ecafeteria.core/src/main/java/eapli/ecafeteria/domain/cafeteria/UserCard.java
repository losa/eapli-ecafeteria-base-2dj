/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import eapli.framework.domain.Money;
import java.util.Observable;

/**
 * TODO assuming UserCard is part of the CafeteriaUser aggregate. so no external
 * reference to UserCard objects should exist. all references should be to the
 * aggregate root
 *
 * @author mcn
 */
@Entity
public class UserCard extends Observable implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long id;


    @OneToMany
    private List<CardMovement> movements;
    @Temporal(TemporalType.DATE)
    private final Calendar createdOn;

    public UserCard() {
        movements = new ArrayList<>();
        createdOn = Calendar.getInstance();
    }

    public List<CardMovement> movements() {
        return Collections.unmodifiableList(movements);
    }

    public Money balance() {
        Money result = Money.euros(0);
        for (final CardMovement mov : movements) {
            result = result.add(mov.accountingValue());
        }
        return result;
    }

    public synchronized void addMovement(CardMovement cm) {
        this.movements.add(cm);
        notifyAll();
    }
}
