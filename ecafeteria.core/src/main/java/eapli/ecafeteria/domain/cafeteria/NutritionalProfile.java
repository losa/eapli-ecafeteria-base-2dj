/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import eapli.framework.domain.ddd.ValueObject;
import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author Rui Peixoto
 */
@Embeddable
public class NutritionalProfile implements ValueObject, Serializable {

    private static final long serialVersionUID = 1L;

    private Integer maxCaloriesPerMeal;
    private Integer maxSaltPerMeal;
    private Integer maxSaltPerWeek;
    private Integer maxCaloriesPerWeek;

    public NutritionalProfile(Integer maxCaloriesPerMeal, Integer maxSaltPerMeal, Integer maxCaloriesPerWeek, Integer maxSaltPerWeek) {

        if (maxCaloriesPerMeal == null || maxCaloriesPerMeal <= 0) {
            throw new IllegalStateException("Max calories per meal can't be negative or equal 0");
        }
        if (maxSaltPerMeal == null || maxSaltPerMeal < 0) {
            throw new IllegalStateException("Max salt per salt can't be negative");
        }
        if (maxCaloriesPerWeek == null || maxCaloriesPerWeek <= 0) {
            throw new IllegalStateException("Max calories per meal can't be negative or equal 0");
        }
        if (maxSaltPerWeek == null || maxSaltPerWeek < 0) {
            throw new IllegalStateException("Max salt per salt can't be negative");
        }
        this.maxCaloriesPerMeal = maxCaloriesPerMeal;
        this.maxSaltPerMeal = maxSaltPerMeal;
        this.maxCaloriesPerWeek = maxCaloriesPerWeek;
        this.maxSaltPerWeek = maxSaltPerWeek;

    }

    protected NutritionalProfile() {
        //for ORM
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NutritionalProfile)) {
            return false;
        }

        final NutritionalProfile that = (NutritionalProfile) o;

        if (!that.maxCaloriesPerMeal.equals(this.maxCaloriesPerMeal)) {
            return false;
        }
        if (!this.maxCaloriesPerWeek.equals(that.maxCaloriesPerWeek)) {
            return false;
        }
        if (!this.maxSaltPerMeal.equals(that.maxSaltPerMeal)) {
            return false;
        }
        return this.maxSaltPerWeek.equals(that.maxSaltPerWeek);
    }

    public NutritionalProfile changeNutritionalProfile(Integer maxCaloriesPerMeal, Integer maxSaltPerMeal, Integer maxCaloriesPerWeek, Integer maxSaltPerWeek) {
        if (maxCaloriesPerMeal == null || maxCaloriesPerMeal <= 0) {
            throw new IllegalStateException("Max calories per meal can't be negative or equal 0");
        }
        if (maxSaltPerMeal == null || maxSaltPerMeal < 0) {
            throw new IllegalStateException("Max salt per salt can't be negative");
        }
        if (maxCaloriesPerWeek == null || maxCaloriesPerWeek <= 0) {
            throw new IllegalStateException("Max calories per meal can't be negative or equal 0");
        }
        if (maxSaltPerWeek == null || maxSaltPerWeek < 0) {
            throw new IllegalStateException("Max salt per salt can't be negative");
        }
        this.maxCaloriesPerMeal = maxCaloriesPerMeal;
        this.maxSaltPerMeal = maxSaltPerMeal;
        this.maxCaloriesPerWeek = maxCaloriesPerWeek;
        this.maxSaltPerWeek = maxSaltPerWeek;
        return this;
    }

    @Override
    public String toString() {
        return "Maximum Calories Per Meal: " + maxCaloriesPerMeal
                + "\nMaximum Salt Per Meal: " + maxSaltPerMeal
                + "\nMaximum Calories Per Week: " + maxCaloriesPerWeek
                + "\nMaximum Salt Per Week: " + maxSaltPerWeek + "\n";
    }

    public Integer weeklyCaloricIntake() {
        //TODO
        return 0; // weeklyIngestedCalories
    }

}
