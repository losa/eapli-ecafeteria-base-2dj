/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.ecafeteria.domain.cafeteria;

import eapli.ecafeteria.application.cafeteria.CheckWarningsUserBalanceController;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class BalanceWatchDog extends Observable implements Observer{

    private CheckWarningsUserBalanceController theController = new CheckWarningsUserBalanceController();
    
    @Override
    public void update(Observable o, Object o1) {
        UserCard userCard = (UserCard) o;
        if(theController.checkIfUserBalanceNeedsWarning(userCard)) {
            notifyAll();
        }
    }

}
