/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.domain.cafeteria;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.framework.domain.ddd.ValueObject;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Rui Peixoto
 */
@Entity
public class UserAllergen implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private CafeteriaUser cafeteriaUser;

    @ManyToOne
    private Allergen allergen;

    public UserAllergen(CafeteriaUser cafeteriaUser, Allergen allergen) {
        if (cafeteriaUser == null || allergen == null) {
            throw new IllegalStateException();
        }
        this.cafeteriaUser = cafeteriaUser;
        this.allergen = allergen;
    }

    protected UserAllergen() {
        //For ORM
    }

    public Allergen allergen() {
        return this.allergen;
    }

    public Long id() {
        return this.id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserAllergen)) {
            return false;
        }

        final UserAllergen other = (UserAllergen) o;
        return (this.cafeteriaUser.equals(other.cafeteriaUser) && this.allergen.equals(other.allergen));
    }
}
