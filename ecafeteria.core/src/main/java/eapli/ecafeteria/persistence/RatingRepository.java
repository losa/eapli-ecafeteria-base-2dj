/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.user.Rating;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author Miguel Minnemann (1150457) & Daniel Ribeiro (1150577)
 */
public interface RatingRepository extends DataRepository<Rating, Long> {

    Iterable<Rating> findAllRatingsByUser(SystemUser user);

    Iterable<Rating> findAllRatingsByMeal(Meal meal);
    
    Iterable<Rating> findAllRatingsByDish(Dish dish);
}
