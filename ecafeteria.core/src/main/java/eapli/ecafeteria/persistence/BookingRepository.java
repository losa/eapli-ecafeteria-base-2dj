/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.user.Booking;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Calendar;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public interface BookingRepository extends DataRepository<Booking, Long> {

    Iterable<Booking> findAllBookingsByUser(CafeteriaUser user);

    Iterable<Booking> findAllBookingsByMeal(Meal meal);

    Iterable<Booking> findAllBookingsByDish(Dish dish);

    Iterable<Booking> findAllBookingsByDishType(DishType dishType);

    Iterable<Booking> findAllBookingsByDay(Calendar date);

    Iterable<Booking> allBookingsDeliveredToUser(CafeteriaUser user);

    Iterable<Booking> findAllBookingsByDayAndMealType(Calendar date, MealType mealType);
}
