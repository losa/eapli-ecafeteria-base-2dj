/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.complaints.Complaint;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author gustavo (1140320), Vasco (1140338)
 */
public interface ComplaintRepository extends DataRepository<Complaint, Long>{

    Iterable<Complaint> findAllComplaintsByUser(SystemUser user);

    Iterable<Complaint> findAllComplaintsByDish(Dish dish);
}
