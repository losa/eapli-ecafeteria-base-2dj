/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.kitchen.Lot;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public interface LotRepository extends DataRepository<Lot, Long>{
    
    public Lot findByAcronym(String lotCode);
}
