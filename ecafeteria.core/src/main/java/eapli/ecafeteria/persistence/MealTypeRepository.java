package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.repositories.DataRepository;

public interface MealTypeRepository extends DataRepository<MealType, Long>{
    
    MealType findByName(Designation name);
}
