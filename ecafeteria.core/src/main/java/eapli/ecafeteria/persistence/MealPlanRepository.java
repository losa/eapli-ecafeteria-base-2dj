package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Calendar;
import java.util.List;

/**
 * Repository for Meal Plans
 * 
 */
public interface MealPlanRepository extends DataRepository<MealPlan, Long> {
    
    List<Meal> findMealPlanByDate(Calendar date);
    
    Iterable<MealPlan> findValidMealPlans();
    
}
