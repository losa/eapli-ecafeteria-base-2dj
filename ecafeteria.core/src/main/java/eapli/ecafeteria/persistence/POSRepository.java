/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Date;

/**
 *
 * @author Nuno Meireles <1150830@isep.ipp.pt>
 * @author José Guimarães <1150812@isep.ipp.pt>
 */
public interface POSRepository extends DataRepository<POS, Long> {
    
    public POS findByDate(Date date);
}
