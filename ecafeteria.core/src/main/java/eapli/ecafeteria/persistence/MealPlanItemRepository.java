/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Calendar;

/**
 *
 * @author jferr
 */
public interface MealPlanItemRepository extends DataRepository<MealPlanItem, Long> {
    
    Iterable<MealPlanItem> findMealPlanItemByDateAndType(Calendar date, MealType type);
    
    Iterable<MealPlanItem> findMealPlanItemsByMealPlan(MealPlan mealPlan);
    
}
