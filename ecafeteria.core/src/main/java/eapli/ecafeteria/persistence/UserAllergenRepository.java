/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.UserAllergen;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.framework.persistence.repositories.DataRepository;

/**
 *
 * @author Rui Peixoto
 */
public interface UserAllergenRepository extends DataRepository<UserAllergen, Long> {

    Iterable<UserAllergen> findAllergenByCafeteriaUser(CafeteriaUser cafeteriaUser);
    
    UserAllergen checkIfAllergenIsRegisted(CafeteriaUser cafeteriaUser, Allergen allergen);
}
