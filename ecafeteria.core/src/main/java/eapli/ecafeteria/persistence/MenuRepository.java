package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.persistence.repositories.DataRepository;

public interface MenuRepository extends DataRepository<Menu, Long>  {
    
    Iterable<Menu> publishedMenus();
    
    Iterable<Menu> unpublishedMenus();
    
    Iterable<Menu> thisWeekMenus();
    
}
