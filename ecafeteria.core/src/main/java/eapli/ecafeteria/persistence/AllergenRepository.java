/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.repositories.DataRepository;

/**
 * @author Luis Oliveira 1150773
 * @author José Silva 1150371
 */
public interface AllergenRepository extends DataRepository<Allergen, String> {

    Allergen findByName(String name);
}
