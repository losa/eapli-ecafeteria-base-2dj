/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.persistence.repositories.DataRepository;
import java.util.Calendar;

/**
 *
 * @author jferr
 */
public interface MealRepository extends DataRepository<Meal, Long> {
    
    Iterable<Meal> findByPeriod(Calendar startingDate, Calendar endDate);
    
    Iterable<Meal> findByMenu(Menu menu);
    
    Iterable<Meal> findPublishedMealsInPeriod(Calendar startingDate, Calendar endDate);
    
    Iterable<Meal> findByDate(Calendar date);
    
    Iterable<Meal> findByDish(Dish d);
    
    Iterable<Meal> findByMealType(MealType mealType);
    
}
