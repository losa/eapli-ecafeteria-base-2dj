package eapli.cafeteria.consoleapp.presentation;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import eapli.ecafeteria.application.cafeteria.ChangeNutritionalProfileController;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rui Peixoto
 */
public class ChangeNutritionalProfileUI extends AbstractUI {

    private final ChangeNutritionalProfileController controller = new ChangeNutritionalProfileController();
    
    protected Controller controller() {
        return this.controller;
    }
    
    @Override
    protected boolean doShow() {
       final int maxCaloriesPerMeal = Console.readInteger("Maximum calories per meal");
       final int maxCaloriesPerWeek = Console.readInteger("Maximum calories per week");
       final int maxSaltPerMeal= Console.readInteger("Maximum salt per meal");
       final int maxSaltPerWeek = Console.readInteger("Maximum salt per week");
       
        try {
            controller.changeNutritionalProfile(maxCaloriesPerMeal, maxCaloriesPerWeek, maxSaltPerMeal, maxSaltPerWeek);
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(ChangeNutritionalProfileUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(ChangeNutritionalProfileUI.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       return true;
    }

    @Override
    public String headline() {
        return "Change Nutritional Profile:";
    }
    
}
