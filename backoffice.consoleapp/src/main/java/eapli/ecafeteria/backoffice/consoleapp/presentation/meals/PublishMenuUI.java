/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.PublishMenuController;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nuno Meireles <1150830@isep.ipp.pt>
 * @author José Guimarães <1150812@isep.ipp.pt>
 */
public class PublishMenuUI extends AbstractUI{
    
    public PublishMenuUI() {
    }

    @Override
    protected boolean doShow() {
        PublishMenuController theController = new PublishMenuController();
        try {
            final Iterable<Menu> menus = theController.listMenus();
            
            final SelectWidget<Menu> selector = new SelectWidget<>("Menus:", menus, new MenuPrinter());
            selector.show();
            final Menu theMenu = selector.selectedElement();
            
            if (theMenu != null) {
                
                if(theController.publishMenu(theMenu)){
                    System.out.println("Menu publicado");
                }else{
                    System.out.println("Menu não publicado");
                }   
            }         
        }   catch (DataIntegrityViolationException | DataConcurrencyException ex) {
            Logger.getLogger(PublishMenuUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public String headline() {
        return "Publish Menu";
    }
    
}
