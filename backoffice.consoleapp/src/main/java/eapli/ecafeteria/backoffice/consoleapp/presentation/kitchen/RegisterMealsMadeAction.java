
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.framework.actions.Action;

public class RegisterMealsMadeAction implements Action{

    @Override
    public boolean execute() {
        return new RegisterMealsMadeUI().doShow();
    }
    
}
