/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author vasco
 */
public class MealPlanItemPrinter implements Visitor<MealPlanItem> {

    @Override
    public void visit(MealPlanItem visitee) {
        System.out.printf("%-30s%-25s%-10s%-4s\n", visitee.meal().date(),
                visitee.meal().dishName(), visitee.meal().mealType(), visitee.amount());
    }
}
