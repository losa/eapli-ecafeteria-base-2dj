/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.kitchen.ShowMealsRatingController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.user.Comment;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;

/**
 *
 * @author Daniel Ribeiro & Miguel Minnemann (1150577 & 1150457)
 */
public class ShowMealRatingUI extends AbstractUI {

    private final ShowMealsRatingController theController = new ShowMealsRatingController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final Iterable<Meal> meals = this.theController.allMealsRated();

        final SelectWidget<Meal> selector = new SelectWidget<>("Select the meal to see rating", meals, new MealPrinter());
        selector.show();
        final Meal theMealSelected = selector.selectedElement();

        if (theMealSelected == null) {
            return true;
        }

        try {
            this.theController.findAllRatingByMeal(theMealSelected);
            final int numRatings = this.theController.numberRatingOfMeal();
            final float averageRatings = this.theController.averageMealRating();
            
            System.out.println("\nRatings Numbers: " + numRatings);
            System.out.println("Average Rating: " + averageRatings + "\n");

            final Iterable<Comment> comments = this.theController.mealComments();
            
            final SelectWidget<Comment> selectorComment = new SelectWidget<>("Do you wish to reply to a comment?", comments, new CommentPrinter());
            selectorComment.show();
            final Comment theCommentSelected = selectorComment.selectedElement();
            
            if(theCommentSelected == null) {
                return false;
            }
            
            final String resposta = Console.readLine("The reply: ");
            
            this.theController.addReplyToComment(theCommentSelected, resposta);
            return true;
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            System.out.println("This meal was already rated by you");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Rate Meal";
    }
}
