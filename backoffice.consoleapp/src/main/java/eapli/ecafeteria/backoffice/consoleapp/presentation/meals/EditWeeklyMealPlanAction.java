package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.framework.actions.Action;

/**
 *
 * @author vasco
 */
public class EditWeeklyMealPlanAction implements Action {

    @Override
    public boolean execute() {
        return new EditWeeklyMealPlanUI().show();
    }
}
