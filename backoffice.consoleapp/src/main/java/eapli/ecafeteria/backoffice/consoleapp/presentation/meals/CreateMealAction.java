
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.actions.Action;

public class CreateMealAction implements Action {

    private final CreateMealUI createMenuUI;
    
    public CreateMealAction(Menu menu){
        this.createMenuUI = new CreateMealUI(menu);
    }
    
    @Override
    public boolean execute() {
        return createMenuUI.doShow();
    }
    
}
