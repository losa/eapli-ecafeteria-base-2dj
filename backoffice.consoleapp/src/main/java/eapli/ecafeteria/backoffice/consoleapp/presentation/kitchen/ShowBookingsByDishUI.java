/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.ShowBookingsByDishController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.user.Booking;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ListWidget;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class ShowBookingsByDishUI extends AbstractUI {

    private final ShowBookingsByDishController theController = new ShowBookingsByDishController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        Iterable<Dish> dishes = theController.allDishes();

        final SelectWidget<Dish> selector = new SelectWidget<>("Select the meal to see rating", dishes, new DishPrinter());
        selector.show();
        final Dish theDishSelected = selector.selectedElement();

        if (theDishSelected == null) {
            return true;
        }

        ListWidget<Booking> listWidgetBookings = listWidgetBookings 
                = new ListWidget<>("Bookings:", theController.findAllBookingsByDish(theDishSelected), new BookingPrinter());
        listWidgetBookings.show();
        Console.waitForKey("Press any key to exit");
        return false;
    }

    @Override
    public String headline() {
        return "Show Bookings By Dish";
    }
}
