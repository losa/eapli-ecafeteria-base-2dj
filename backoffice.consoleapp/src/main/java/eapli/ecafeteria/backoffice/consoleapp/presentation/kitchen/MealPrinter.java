package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.domain.meals.Meal;
import eapli.framework.visitor.Visitor;

public class MealPrinter implements Visitor<Meal> {

    @Override
    public void visit(Meal visitee) {
        System.out.printf("%-30s%-25s%-10s\n", visitee.date().getTime(), visitee.dishName(), visitee.mealType());
    }
    
}
