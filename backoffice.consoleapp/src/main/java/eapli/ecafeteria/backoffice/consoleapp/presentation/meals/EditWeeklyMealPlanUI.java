package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.EditWeeklyMealPlanController;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vasco
 */
public class EditWeeklyMealPlanUI extends AbstractUI {

    private final EditWeeklyMealPlanController theController = new EditWeeklyMealPlanController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        try {
            final Iterable<MealPlan> allMealPlans = this.theController.allMealPlans();
            final SelectWidget<MealPlan> selector1 = new SelectWidget<>("Select a Meal Plan: ", allMealPlans, new MealPlanPrinter());
            selector1.show();
            final MealPlan updtMealPlan = selector1.selectedElement();
            try {
                final Iterable<MealPlanItem> allMealPlanItems = this.theController.mealPlanItemsFromAMealPlan(updtMealPlan);
                final SelectWidget<MealPlanItem> selector2 = new SelectWidget<>("Select a Meal Plan Item: ",allMealPlanItems, new MealPlanItemPrinter());
                selector1.show();
                final MealPlanItem updtMealPlanItem = selector2.selectedElement();
                try {
                    final int newAmount = Console.readInteger("New Meal Amount");
                    this.theController.editMealPlanItem(updtMealPlanItem, newAmount);
                    this.theController.editMealPlan(updtMealPlan);
                } catch (DataConcurrencyException ex) {
                    System.out.println("It is not possible to change the meal plan item because it was changed by another user");
                } catch (DataIntegrityViolationException ex) {
                    //should not happen!
                    Logger.getLogger(EditWeeklyMealPlanUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (NullPointerException ex) {
                System.out.println("There are no registered Meal Plan Items");
            }
        } catch (NullPointerException ex) {
            System.out.println("There are no registered Meal Plans");
        }
        return true;
    }

    @Override
    public String headline() {
        return "Edit Weekly Meal Plan";
    }
}
