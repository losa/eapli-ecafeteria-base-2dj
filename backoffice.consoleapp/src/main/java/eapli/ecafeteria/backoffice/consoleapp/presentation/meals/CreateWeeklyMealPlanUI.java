/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.CreateWeeklyMealPlanController;
import eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen.MealPrinter;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.DateTime;
import eapli.util.io.Console;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jferr
 */
public class CreateWeeklyMealPlanUI extends AbstractUI {

    private final CreateWeeklyMealPlanController theController = new CreateWeeklyMealPlanController();

    protected Controller controller() {
        return this.theController;
    }

    /**
     * FIX ME Meal selector should loop to allow several meals to be added to
     * the MealPlan
     *
     * @return
     */
    @Override
    protected boolean doShow() {
        try {

            int mealCount = 0;
            Date day = null;

            day = Console.readDate("Introduce a date (YYYY/MM/DD):");

            System.out.println("");

            final String mealPlanName = Console.readLine("Name your Meal Plan: ");

            System.out.println("");

            final Iterable<Meal> mealsInAWeek = this.theController.mealsInAWeek(day);

            Iterator<Meal> it = mealsInAWeek.iterator();

            if (!it.hasNext()) {
                System.out.println("No meals in selected week!\n");
            } else {

                Meal selectedMeal;
                MealPlan mealPlan = null;

                try {
                    mealPlan = this.theController.saveMealPlan(mealPlanName, day);
                    System.out.println("Meal plan saved successfully! Add your meals and amounts.");

                } catch (DataConcurrencyException ex) {
                    System.out.println("Meal Plan already exists!");
                } catch (DataIntegrityViolationException ex) {
                    //should not happen
                    Logger.getLogger(CreateWeeklyMealPlanUI.class.getName()).log(Level.SEVERE, null, ex);
                }

                System.out.println("");

                do {
                    final SelectWidget<Meal> selector1 = new SelectWidget<>("Meals Available: ", mealsInAWeek, new MealPrinter());
                    selector1.show();
                    selectedMeal = selector1.selectedElement();

                    if (selectedMeal != null) {
                        final int amount = Console.readInteger("Amount:");

                        try {
                            this.theController.registerMealPlanItem(mealPlan, selectedMeal, amount);
                            mealCount++;
                            System.out.println("");
                        } catch (DataConcurrencyException ex) {
                            System.out.println("Meal and amount already selected!");
                        } catch (DataIntegrityViolationException ex) {
                            Logger.getLogger(CreateWeeklyMealPlanUI.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }

                } while (selectedMeal != null || mealCount == 0);

                System.out.println("Meal Plan completed!\n");
            }

        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

        return true;
    }

    @Override
    public String headline() {
        return "Create Weekly Meal Plan";
    }

}
