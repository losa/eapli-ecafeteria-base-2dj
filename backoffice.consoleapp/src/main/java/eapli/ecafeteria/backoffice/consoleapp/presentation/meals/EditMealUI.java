package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.EditMealController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.framework.presentation.console.ShowMessageAction;
import eapli.util.io.Console;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EditMealUI extends AbstractUI {

    private final EditMealController editMealController;
    
    public EditMealUI(Menu menu){
        this.editMealController = new EditMealController(menu);
    }
    
    @Override
    protected boolean doShow() {
        
        final SelectWidget<Meal> mealSelector = new SelectWidget<>("\nMeals:\n",editMealController.getMeals(), new MealPrinter());
        mealSelector.show();

        final Meal meal = mealSelector.selectedElement();
        if (meal == null){
            return true;
        }
        final Calendar newDate = Console.readCalendar("\nNew date (dd-mm-yyyy)");

        final SelectWidget<Dish> dishSelector = new SelectWidget<>("\nDishes:\n",editMealController.getDishes(), new DishPrinter());
        dishSelector.show();
        final Dish newDish = dishSelector.selectedElement();
        if (newDish == null){
            return true;
        }

        final SelectWidget<MealType> mealTypeSelector = new SelectWidget<>("\nMeal Types:\n",editMealController.getMealTypes(), new MealTypePrinter());
        mealTypeSelector.show();
        final MealType newMealType = mealTypeSelector.selectedElement();
        if (newMealType == null){
            return true;
        }
            
        try {    
            if (editMealController.editMeal(meal, newDate, newDish, newMealType)){
                new ShowMessageAction("Meal edited sucefully").execute();
            }
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            Logger.getLogger(EditMealUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return true;
    }

    @Override
    public String headline() {
        return "Edit Menu";
    }
    
}
