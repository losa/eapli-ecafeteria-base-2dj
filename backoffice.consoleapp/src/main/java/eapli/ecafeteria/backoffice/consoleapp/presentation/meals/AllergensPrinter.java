/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author 1150831
 */
public class AllergensPrinter implements Visitor<Allergen> {

    @Override
    public void visit(Allergen visitee) {
        System.out.printf("%-10s%-30s%-4s\n", visitee.id(), visitee.name(), String.valueOf(visitee.isActive()));
    }

    @Override
    public void beforeVisiting(Allergen visitee) {
        //nothing to do
    }

    @Override
    public void afterVisiting(Allergen visitee) {
        //nothing to do
    }
}
