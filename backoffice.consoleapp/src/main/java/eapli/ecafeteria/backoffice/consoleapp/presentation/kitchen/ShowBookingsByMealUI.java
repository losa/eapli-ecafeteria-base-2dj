/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.ShowBookingsByMealController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.user.Booking;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ListWidget;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class ShowBookingsByMealUI extends AbstractUI {

    private final ShowBookingsByMealController theController = new ShowBookingsByMealController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        Iterable<Meal> dishes = theController.allMeals();

        final SelectWidget<Meal> selector = new SelectWidget<>("Select the meal to see rating", dishes, new MealPrinter());
        selector.show();
        final Meal theMealSelected = selector.selectedElement();

        if (theMealSelected == null) {
            return true;
        }

        ListWidget<Booking> listWidgetBookings
                = new ListWidget<>("Bookings:", theController.findAllBookingsByMeal(theMealSelected), new BookingPrinter());
        listWidgetBookings.show();
        Console.waitForKey("Press any key to exit");
        return false;
    }

    @Override
    public String headline() {
        return "Show Bookings By Meal";
    }
}
