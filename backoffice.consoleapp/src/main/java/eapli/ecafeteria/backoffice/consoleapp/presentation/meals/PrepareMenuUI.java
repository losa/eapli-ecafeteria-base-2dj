package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.ListMenuController;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.domain.TimePeriod2;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;
import java.util.Calendar;
import java.util.List;

public class PrepareMenuUI extends AbstractUI {

    private final ListMenuController listMenuController = new ListMenuController();
    private final MenuPrinter menuPrinter = new MenuPrinter();
   
    @Override
    protected boolean doShow() {
        final MenuRenderer renderer;
        Calendar initialDate;
        Calendar finalDate;
        
        do{
            initialDate = Console.readCalendar("\nPeriod initial date (dd-mm-yyyy)");
            finalDate = Console.readCalendar("\nPeriod final date (dd-mm-yyyy)");
            if (initialDate.compareTo(finalDate)>0){
                System.out.println("Final date can't be before the initial date");
            }
        }while(initialDate.compareTo(finalDate)>0);
        
        final List<Menu> menus = listMenuController.getUnpublishedMenusByPeriod(initialDate, finalDate);
        Menu menu;
        if (menus.isEmpty()){
            final String menuDescription = Console.readLine("\nNew Menu description");
            menu = new Menu(menuDescription, new TimePeriod2(initialDate, finalDate));
        }else{
            SelectWidget<Menu> selector = new SelectWidget<>("Menus:\n",menus, menuPrinter);
            selector.show();
            menu = selector.selectedElement();
        }
        
        String opcao;
        
        do{
            System.out.println("\n1 - Add Meal");
            System.out.println("2 - Edit Meal");
            System.out.println("0 - Return");
            
            opcao = Console.readLine("Choose one option: ");
            switch(opcao){
                case "1":
                    new CreateMealAction(menu).execute();
                    break;
                case "2":
                    new EditMealAction(menu).execute();
                    break;
                case "0":
                    break;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }while(!opcao.equals("0"));
        
        return true;
    }

    @Override
    public String headline() {
        return "Menu";
    }
    
}
