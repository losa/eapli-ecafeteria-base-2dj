package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.visitor.Visitor;

public class MealTypePrinter implements Visitor<MealType>{

    @Override
    public void visit(MealType visitee) {
        System.out.printf("%-30s\n", visitee.name());
    }
    
}
