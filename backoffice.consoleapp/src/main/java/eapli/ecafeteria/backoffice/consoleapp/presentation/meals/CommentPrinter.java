/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.user.Comment;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Daniel
 */
class CommentPrinter implements Visitor<Comment> {

    @Override
    public void visit(Comment visitee) {
        System.out.printf("%-30s\n", visitee.comment());
    }

}
