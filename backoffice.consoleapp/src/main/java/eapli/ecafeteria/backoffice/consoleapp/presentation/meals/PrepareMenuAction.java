package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.framework.actions.Action;

public class PrepareMenuAction implements Action{

    @Override
    public boolean execute() {
        return new PrepareMenuUI().doShow();
    }
    
}
