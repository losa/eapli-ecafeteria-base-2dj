package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.RegisterMealsMadeController;
import eapli.ecafeteria.application.meals.ListDishService;
import eapli.ecafeteria.backoffice.consoleapp.presentation.meals.MealTypePrinter;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegisterMealsMadeUI extends AbstractUI {

    private final RegisterMealsMadeController registerMealsMadeController = new RegisterMealsMadeController();
    
    private final MealPrinter mealPrinter = new MealPrinter();
    
    private final DishPrinter dishPrinter = new DishPrinter();
    
    private final MealTypePrinter mealTypePrinter = new MealTypePrinter();

    @Override
    protected boolean doShow() {
        ArrayList filter = new ArrayList();
        filter.add("Date");
        filter.add("MealType");
        filter.add("Dish");

        String typeFilter;
        Dish d;
        Calendar date;
        MealType mealType;
        ArrayList<Meal> meals = null;

        SelectWidget<String> selector = new SelectWidget<>("Get Meals By", filter);
        selector.show();
        typeFilter = selector.selectedElement();

        switch (typeFilter) {
            case "Date":
                date = Console.readCalendar("Date");
                meals = (ArrayList<Meal>) registerMealsMadeController.getMealsByDate(date);
                break;

            case "MealType":
                Iterable<MealType> mealTypes = registerMealsMadeController.getMealTypes();
                SelectWidget<MealType> selectorMealTypes = new SelectWidget<>("MealTypes", mealTypes, mealTypePrinter);
                selectorMealTypes.show();
                mealType = selectorMealTypes.selectedElement();
                meals = (ArrayList<Meal>) registerMealsMadeController.getMealsByMealtype(mealType);
                break;

            case "Dish":
                Iterable<Dish> dishes = registerMealsMadeController.allDishes();
                SelectWidget<Dish> selectorDish = new SelectWidget<>("Dishes", dishes, dishPrinter);
                selectorDish.show();
                d = selectorDish.selectedElement();
                meals = (ArrayList<Meal>) registerMealsMadeController.getMealsByDish(d);
                break;
        }

        SelectWidget<Meal> selectorMeals = new SelectWidget<>("Meals", meals, mealPrinter);
        selectorMeals.show();
        Meal meal = selectorMeals.selectedElement();
        
        int numberMealsMade = Console.readInteger("Number of Meals Made");
        
        try {
            registerMealsMadeController.RegisterNumberMealsMade(meal, numberMealsMade);
        } catch (DataConcurrencyException| DataIntegrityViolationException ex) {
            Logger.getLogger(RegisterMealsMadeUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public String headline() {
        return "Register Number Meals Made";
    }
}
