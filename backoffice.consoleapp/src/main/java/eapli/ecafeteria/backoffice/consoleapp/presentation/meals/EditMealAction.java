package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.actions.Action;

public class EditMealAction implements Action{

    private final EditMealUI editMenuUI;
    
    public EditMealAction(Menu menu){
        this.editMenuUI = new EditMealUI(menu);
    }
    
    @Override
    public boolean execute() {
        return editMenuUI.doShow();
    }
    
}
