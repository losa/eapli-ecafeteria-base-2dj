/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author Nuno Meireles <1150830@isep.ipp.pt>
 * @author José Guimarães <1150812@isep.ipp.pt>
 */
public class MenuPrinter implements Visitor<Menu>{
    
    // Not Completed, needs menu description and/or period
    @Override
    public void visit(Menu visitee) {
        System.out.printf("Menu description: %s Published: %-5s\n", visitee.description(), visitee.isPublished());
    }

    @Override
    public void beforeVisiting(Menu visitee) {
        // nothing to do
    }

    @Override
    public void afterVisiting(Menu visitee) {
        // nothing to do
    }
}
