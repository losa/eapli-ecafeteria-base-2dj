
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.CreateMealController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.framework.actions.ShowMessageAction;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CreateMealUI extends AbstractUI {

    private final CreateMealController controller;

    public CreateMealUI(Menu menu) {
        this.controller = new CreateMealController(menu);
    }

    @Override
    protected boolean doShow() {

        final Calendar date = Console.readCalendar("\nNew date (dd-mm-yyyy)");

        final SelectWidget<Dish> dishSelector = new SelectWidget<>("\nDishes:\n",controller.getDishes(), new DishPrinter());
        dishSelector.show();
        final Dish dish = dishSelector.selectedElement();
        if (dish == null){
            return true;
        }

        final SelectWidget<MealType> mealTypeSelector = new SelectWidget<>("\nMeal Types:\n",controller.getMealTypes(), new MealTypePrinter());
        mealTypeSelector.show();
        final MealType mealType = mealTypeSelector.selectedElement();
        if (mealType == null){
            return true;
        }

        try {
            if (controller.createMeal(date, dish, mealType)) {
                new ShowMessageAction("Meal created successfuly").execute();
            }
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            Logger.getLogger(EditMealUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        return true;
    }

    @Override
    public String headline() {
        return "Create Menu";
    }

}
