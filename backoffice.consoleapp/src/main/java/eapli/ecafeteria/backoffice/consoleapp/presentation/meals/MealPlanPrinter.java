package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author vasco
 */
public class MealPlanPrinter implements Visitor<MealPlan> {

    @Override
    public void visit(MealPlan visitee) {
        System.out.printf("%-30s%-30s\n", visitee.beginDate(), visitee.endDate());
    }
}
