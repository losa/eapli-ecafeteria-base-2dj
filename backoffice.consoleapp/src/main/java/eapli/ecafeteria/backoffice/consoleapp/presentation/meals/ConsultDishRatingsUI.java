/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.kitchen.ConsultDishRatingsController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;

/**
 *
 * @author Mariana Calafate(1150481) e Manuel Gerardo(1150480)
 */
public class ConsultDishRatingsUI extends AbstractUI{
    
    private final ConsultDishRatingsController theController = new ConsultDishRatingsController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final Iterable<Dish> dishes = this.theController.allDishes();
      
        final SelectWidget<Dish> selector = new SelectWidget<>("Select the Dish to see its ratings", dishes, new DishPrinter());
        selector.show();
        final Dish theDishSelected = selector.selectedElement();
        
        if(theDishSelected == null) {
            return true;
        }
        
        int count = 0;
        for (Integer key : theController.showRatingsInformation(theDishSelected).keySet()){
            count = key;
        }
        
        double average = theController.showRatingsInformation(theDishSelected).get(count);
        
        System.out.println("The average rating is " + average + " stars for " + count + " ratings.");
        
        return false;
    }

    @Override
    public String headline() {
        return "Consult dish ratings";
    }
}

