/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.kitchen;

import eapli.ecafeteria.application.kitchen.ShowBookingsByDishTypeController;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.user.Booking;
import eapli.framework.application.Controller;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.ListWidget;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class ShowBookingsByDishTypeUI extends AbstractUI {

    private final ShowBookingsByDishTypeController theController = new ShowBookingsByDishTypeController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        Iterable<DishType> dishTypes = theController.allDishTypes();

        final SelectWidget<DishType> selector = new SelectWidget<>("Select the meal to see rating", dishTypes, new DishTypePrinter());
        selector.show();
        final DishType theDishTypeSelected = selector.selectedElement();

        if (theDishTypeSelected == null) {
            return true;
        }

        ListWidget<Booking> listWidgetBookings = listWidgetBookings
                = new ListWidget<>("Bookings:", theController.findAllBookingsByDishType(theDishTypeSelected), new BookingPrinter());
        listWidgetBookings.show();
        Console.waitForKey("Press any key to exit");
        return false;
    }

    @Override
    public String headline() {
        return "Show Bookings By Dish Type";
    }
}
