/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.backoffice.consoleapp.presentation.meals;

import eapli.ecafeteria.application.meals.RegisterAllergenController;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;


/**
 *
 * @author luis
 */
public class RegisterAllergenUI extends AbstractUI {

    private final RegisterAllergenController theController = new RegisterAllergenController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final String name = Console.readLine("Allergen name:");

        try {
            this.theController.registerAllergen(name);
        } catch (final DataConcurrencyException | DataIntegrityViolationException e) {
            System.out.println("That name is already in use.");
        }
        return false;
    }

    @Override
    public String headline() {
        return "Register Allergen";
    }
}
