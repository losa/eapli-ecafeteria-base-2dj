/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.meals.RegisterAllergenController;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Logger;

/**
 *
 * @author luis
 */
public class AllergenBootstraper implements Action {

    @Override
    public boolean execute() {

        final AllergenRepository AllergenRepo = PersistenceContext.repositories().Allergens();
        
        register("Cereais com glúten");
        register("Crustáceos e derivados");
        register("Ovos e derivados");
        register("Peixe e derivados");
        register("Amendoins e derivados");
        register("Soja e derivados");
        register("Leite e derivados");
        register("Frutos de casca rija(avela, amendoas, nozes, etc)");
        register("Aipo e derivados");
        register("Mostarda e derivados");
        register("Sementes de sésamo e derivados");
        register("Dioxido de enxofre e sulfitos em concentrações > 10mg/Kg ou 10ml/L");
        register("Tremoço e derivados");
        register("Molusculos e derivados");
        
        return false;
    }

    /**
     *
     */
    private void register(String description) {
        final RegisterAllergenController controller = new RegisterAllergenController();
        try {
            controller.registerAllergen(description);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }
    }
}
