/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.domain.complaints.Complaint;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.ComplaintRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.persistence.repositories.TransactionalContext;

/**
 *
 * @author gustavo (1140320), Vasco (1140338)
 */
public class ComplaintBootstraper implements Action {

    private final TransactionalContext TxCtx = PersistenceContext.repositories().buildTransactionalContext();
    final private ComplaintRepository complaintRepository = PersistenceContext.repositories().complaints();
    
    @Override
    public boolean execute() {
        final DishRepository dishRepository = PersistenceContext.repositories().dishes();
        final CafeteriaUserRepository userRepository = PersistenceContext.repositories().cafeteriaUsers(TxCtx);
        MecanographicNumber mn = new MecanographicNumber("900330");
        final CafeteriaUser cafeteriaUser = userRepository.findByMecanographicNumber(mn);
        final Dish dish = dishRepository.findByName(Designation.valueOf("tofu grelhado"));
        final String compl = "carne estava pouco cozida";
        
        Complaint complaint = new Complaint(compl, dish, cafeteriaUser);
     
        try { 
            complaintRepository.save(complaint);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            System.out.println("Erro");
        }
        return false;
    }
    
    /**
     *
     */
    private void register(String complaint, Dish dish, SystemUser user) {

    }
}
