/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.users.RateMealController;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.user.Rating;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Logger;

/**
 *
 * @author Daniel
 */
public class RatingBootstraper implements Action {

    @Override
    public boolean execute() {
        MealRepository mealRepository = PersistenceContext.repositories().meals();
        
        Iterable<Meal> meals = mealRepository.findAll();
        Iterator<Meal> mealIterator = meals.iterator();
        Random random = new Random();
        int randomStarsNumber;
        
        while(mealIterator.hasNext()) {
            randomStarsNumber = random.nextInt(4);
            randomStarsNumber++;
            registerRating(randomStarsNumber, mealIterator.next(), "Muito Bom");
        }
        
        return true;
    }

    private Rating registerRating(final int numberStars, final Meal meal, final String comment) {
        RateMealController rmc = new RateMealController();
        Rating rating = null;
        try {
            rating = rmc.registerRating(numberStars, meal);
            if (comment != null || !comment.isEmpty()) {
                rmc.addCommentToRating(rating, comment);
            }
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }

        return rating;
    }
}
