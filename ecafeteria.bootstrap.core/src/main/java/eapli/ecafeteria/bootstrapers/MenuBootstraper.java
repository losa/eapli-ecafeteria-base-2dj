/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.meals.CreateMealController;
import eapli.ecafeteria.application.meals.PublishMenuController;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.domain.Designation;
import eapli.framework.domain.TimePeriod2;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.util.DateTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miguel
 */
public class MenuBootstraper implements Action {

    @Override
    public boolean execute() {
        final DishRepository dishRepo = PersistenceContext.repositories().dishes();
        
        
        final Dish vegie = dishRepo.findByName(Designation.valueOf("tofu grelhado"));
        final Dish peixe = dishRepo.findByName(Designation.valueOf("bacalhau à braz"));
        final Dish carne = dishRepo.findByName(Designation.valueOf("costeleta à salsicheiro"));
        
        final MealTypeRepository mealTypeRepo = PersistenceContext.repositories().mealTypes();
        final MealType mealType1 = mealTypeRepo.findByName(Designation.valueOf("lunch"));
        final MealType mealType2 = mealTypeRepo.findByName(Designation.valueOf("dinner"));
        
        Calendar date1 = DateTime.newCalendar(2017, 10, 15);
        Calendar date2 = DateTime.newCalendar(2017, 10, 16);
        Calendar date3 = DateTime.newCalendar(2017, 10, 17);
        
        Calendar date4 = DateTime.newCalendar(2017, 12, 20);
        Calendar date5 = DateTime.newCalendar(2017, 12, 25);
        Calendar date6 = DateTime.newCalendar(2017, 12, 24);
        Calendar date7 = DateTime.newCalendar(2017, 12, 22);
        
        Menu menu = new Menu("Ementa 1", new TimePeriod2(date1, date3));
        register(menu, date1, vegie, mealType1);
        register(menu, date2, peixe, mealType2);
        register(menu, date3, carne, mealType1);
        
        Menu publishedMenu = new Menu("Ementa 2", new TimePeriod2(date4, date5));
        publish(publishedMenu);
        
        register(publishedMenu, date7, carne, mealType2);
        register(publishedMenu, date6, carne, mealType1);
        register(publishedMenu, date6, peixe, mealType2);
        register(publishedMenu, date5, vegie, mealType1);
        
        return false;
    }

    private void register(Menu menu, Calendar date, Dish dish, MealType mealType) {
        final CreateMealController controller = new CreateMealController(menu);
        try {
            controller.createMeal(date, dish, mealType);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }
    }
    
    private void publish(Menu menu) {
        final PublishMenuController controller = new PublishMenuController();
        
        try {
            controller.publishMenu(menu);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(MenuBootstraper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(MenuBootstraper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
