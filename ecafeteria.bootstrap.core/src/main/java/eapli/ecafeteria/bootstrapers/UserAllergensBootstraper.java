/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.cafeteria.ChangeUserAllergensController;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Logger;

/**
 *
 * @author Rui Peixoto
 */
public class UserAllergensBootstraper implements Action{

    @Override
    public boolean execute() {
        final CafeteriaUserRepository cafeteriaUserRepo = PersistenceContext.repositories().cafeteriaUsers(null);
        final AllergenRepository allergensRepo = PersistenceContext.repositories().Allergens();
        
        final CafeteriaUser cafeteriaUser = cafeteriaUserRepo.findByUsername(new Username("900330"));
        final Allergen cereais = allergensRepo.findByName("Cereais com glúten");
        final Allergen leite = allergensRepo.findByName("Leite e derivados");
        
        
        register(cafeteriaUser,cereais);
        register(cafeteriaUser,leite);
        
        return false;
    }
    
    private void register(CafeteriaUser user, Allergen allergen) {
        final ChangeUserAllergensController controller = new ChangeUserAllergensController();
        try {
            controller.addNewAllergenForBoostrap(user, allergen);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }
    }
}
