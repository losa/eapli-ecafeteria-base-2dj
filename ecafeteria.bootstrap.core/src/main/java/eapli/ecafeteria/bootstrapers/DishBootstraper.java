/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.application.meals.RegisterDishController;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 *
 * @author mcn
 */
public class DishBootstraper implements Action {

    @Override
    public boolean execute() {

        final DishTypeRepository dishTypeRepo = PersistenceContext.repositories().dishTypes();
        final DishType vegie = dishTypeRepo.findByAcronym("vegie");
        final DishType fish = dishTypeRepo.findByAcronym("fish");
        final DishType meat = dishTypeRepo.findByAcronym("meat");

        /**final AllergenRepository allergenRepo = PersistenceContext.repositories().Allergens();
        ArrayList<Allergen> list = new ArrayList<>();
        Designation d1 = Designation.valueOf("Cereais com glúten");
        list.add(allergenRepo.findByName(d1));
        ArrayList<Allergen> list1 = new ArrayList<>();
        Designation d2 = Designation.valueOf("Crustáceos e derivados");
        list.add(allergenRepo.findByName(d2));

        Designation d3 = Designation.valueOf("Peixe e derivados");
        Designation d4 = Designation.valueOf("Tremoço e derivados");
        Designation d5 = Designation.valueOf("Sementes de sésamo e derivados");
        list1.add(allergenRepo.findByName(d3));
        list1.add(allergenRepo.findByName(d4));
        list1.add(allergenRepo.findByName(d5));**/
        
        register(vegie, "tofu grelhado", 10, 1, new ArrayList<Allergen>(), 2.99);
        register(vegie, "lentilhas salteadas", 10, 1, new ArrayList<Allergen>(), 2.85);
        register(fish, "bacalhau à braz", 50, 2, new ArrayList<Allergen>(), 3.99);
        register(fish, "lagosta suada", 50, 2, new ArrayList<Allergen>(), 24.99);
        register(meat, "picanha", 75, 2, new ArrayList<Allergen>(), 4.99);
        register(meat, "costeleta à salsicheiro", 75, 2, new ArrayList<Allergen>(), 3.99);
        return false;
    }

    /**
     *
     */
    private void register(DishType dishType, String description, int cal, int salt, ArrayList<Allergen> allergen, double price) {
        final RegisterDishController controller = new RegisterDishController();
        try {
            controller.registerDish(dishType, description, cal, salt, allergen, price);
        } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
            // ignoring exception. assuming it is just a primary key violation
            // due to the tentative of inserting a duplicated user
            Logger.getLogger(ECafeteriaBootstraper.class.getSimpleName())
                    .info("EAPLI-DI001: bootstrapping existing record");
        }
    }
}
