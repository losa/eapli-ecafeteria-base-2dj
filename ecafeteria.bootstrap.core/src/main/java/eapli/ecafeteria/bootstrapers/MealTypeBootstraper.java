package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.actions.Action;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.persistence.exceptions.DatabaseException;

public class MealTypeBootstraper implements Action{

    @Override
    public boolean execute() {
        MealTypeRepository mealTypeRepo = PersistenceContext.repositories().mealTypes();
        try {
            mealTypeRepo.save(new MealType("lunch"));
            mealTypeRepo.save(new MealType("dinner"));
        } catch (DataConcurrencyException | DataIntegrityViolationException | DatabaseException ex) {
            Logger.getLogger(MealTypeBootstraper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
}
