/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.bootstrapers;

import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.authz.Username;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.domain.user.Booking;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.framework.actions.Action;
import eapli.framework.domain.Designation;
import eapli.framework.domain.TimePeriod2;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.persistence.repositories.TransactionalContext;
import eapli.util.DateTime;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Optional;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class BookingBootstraper implements Action {

    final private BookingRepository bookingRepository = PersistenceContext.repositories().bookings();

    @Override
    public boolean execute() {

        final MealRepository mealRepository = PersistenceContext.repositories().meals();
        final CafeteriaUserRepository userRepository = PersistenceContext.repositories().cafeteriaUsers(null);

        //temporary fix
        final CafeteriaUser cafeteriaUser900330 = userRepository.findByUsername(new Username("900330"));
        final CafeteriaUser cafeteriaUser900331 = userRepository.findByUsername(new Username("900331"));
        final Iterable<Meal> meals = mealRepository.findAll();

        Booking booking;
        Meal auxMeal;

        Iterator<Meal> mealsIterator = meals.iterator();

        while (mealsIterator.hasNext()) {
            auxMeal = mealsIterator.next();
            booking = new Booking(auxMeal, cafeteriaUser900330);
            booking.deliveryBooking();
            try {
                bookingRepository.save(booking);
            } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
                System.out.println("Erro");
            }
            booking = new Booking(auxMeal, cafeteriaUser900331);
            try {
                bookingRepository.save(booking);
            } catch (final DataIntegrityViolationException | DataConcurrencyException e) {
                System.out.println("Erro");
            }
        }

        return false;
    }

    /**
     *
     */
    private void register(Meal meal, CafeteriaUser user) {

    }
}
