/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafetaria.pos.consoleapp.presentation.cash_register;

import eapli.framework.actions.Action;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class ChargeUserCardAction implements Action {

    @Override
    public boolean execute() {
        return new ChargeUserCardUI().show();
    }
}
