/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafetaria.pos.consoleapp.presentation.cash_register;

import eapli.framework.actions.Action;

/**
 *
 * @author < José Miguel - 1150641@isep.ipp.pt >
 */
public class DeliveryMealAction implements Action {

    @Override
    public boolean execute() {
        return new DeliveryMealUI().doShow();
    }

}
