/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafetaria.pos.consoleapp.presentation.cash_register;

import eapli.framework.actions.Action;

/**
 *
 * @author Nuno Meireles <1150830@isep.ipp.pt>
 * @author José Guimarães <1150812@isep.ipp.pt>
 */
public class OpenPOSAction implements Action {

    @Override
    public boolean execute() {
        return new OpenPOSUI().show();
    }
}
