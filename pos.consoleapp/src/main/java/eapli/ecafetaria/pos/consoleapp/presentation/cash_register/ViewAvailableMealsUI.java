/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafetaria.pos.consoleapp.presentation.cash_register;

import eapli.ecafeteria.application.cashier.ViewAvailableMealsController;
import eapli.framework.application.Controller;
import eapli.framework.domain.Designation;
import eapli.framework.presentation.console.AbstractUI;
import java.util.Map;

/**
 *
 * @author manuelgerardo
 */
public class ViewAvailableMealsUI extends AbstractUI {

    private final ViewAvailableMealsController theController = new ViewAvailableMealsController();

    protected Controller controller() {
        return this.theController;
    }
    
    @Override
    protected boolean doShow() {
        Map<Designation, Integer> mealsPlannedByDate = theController.mealsPlannedByDate();
        if(mealsPlannedByDate == null)
            return false;
        
        mealsPlannedByDate.keySet().stream().forEach((key) -> {
            System.out.println("- " + key + ": " + mealsPlannedByDate.get(key));
        });
        
       return true;
       }

    @Override
    public String headline() {
        return "View Available Meals";
    }
    
}
