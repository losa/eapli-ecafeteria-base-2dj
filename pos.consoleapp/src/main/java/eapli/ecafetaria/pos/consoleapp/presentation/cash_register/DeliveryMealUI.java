/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafetaria.pos.consoleapp.presentation.cash_register;

import eapli.ecafeteria.application.meals.DeliveryMealController;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.user.Booking;
import eapli.ecafeteria.pos.consoleapp.presentation.complaints.BookingPrinter;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author < José Miguel - 1150641@isep.ipp.pt >
 */
public class DeliveryMealUI extends AbstractUI {

    private DeliveryMealController controller;

    @Override
    protected boolean doShow() {
        controller = new DeliveryMealController();

        final SelectWidget<Booking> selectorAllergens = new SelectWidget<>("Bookings:",
                controller.getBookingsForToday(), new BookingPrinter());
        selectorAllergens.show();
        final Booking bookingSelected = selectorAllergens.selectedElement();

        if (bookingSelected == null) {
            return false;
        }
        
        System.out.println("Want delivery Meal? (1-Yes, 2-No)");
        if (Console.readOption(1, 2, 0) == 1) {
            try {
                controller.deliveryMeal(bookingSelected);
            } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
                Logger.getLogger(OpenPOSUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("Meal delivered!");
        }
        
        return false;
    }

    @Override
    public String headline() {
        return "Delivery Meal";
    }

}
