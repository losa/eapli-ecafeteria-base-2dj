/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafetaria.pos.consoleapp.presentation.cash_register;

import eapli.framework.actions.Action;

/**
 *
 * @author Pedro Sousa <1150831@isep.ipp.pt>
 * @author Nelson Palmas <1150829@isep.ipp.pt>
 */
public class ClosePOSAction implements Action{
    
    @Override
    public boolean execute() {
        return new ClosePOSUI().show();
    }
}
