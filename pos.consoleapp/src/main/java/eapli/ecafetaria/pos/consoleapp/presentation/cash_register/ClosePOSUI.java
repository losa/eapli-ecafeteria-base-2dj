/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafetaria.pos.consoleapp.presentation.cash_register;

import eapli.ecafeteria.application.cafeteria.ClosePOSController;
import eapli.ecafeteria.domain.user.Booking;
import eapli.framework.presentation.console.AbstractUI;
import java.util.ArrayList;

/**
 *
 * @author 1150831
 */
public class ClosePOSUI extends AbstractUI {

    private ClosePOSController controller = new ClosePOSController();

    @Override
    protected boolean doShow() {
        ArrayList<Booking> deliveredBookings = new ArrayList();
        deliveredBookings = controller.UndeliveredMeals();

        if (deliveredBookings.isEmpty()) {
            System.out.println("No last minute meals sold.");
        } else {
            for (Booking deliveredBooking : deliveredBookings) {
                System.out.println(deliveredBooking.toString());

            }
        }
        return false;
    }

    @Override
    public String headline() {
        return "Close POS";
    }

}
