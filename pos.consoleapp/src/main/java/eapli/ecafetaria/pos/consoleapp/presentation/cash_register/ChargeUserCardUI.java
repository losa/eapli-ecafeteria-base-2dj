/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafetaria.pos.consoleapp.presentation.cash_register;

import eapli.ecafeteria.application.cashier.ChargeUserCardController;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class ChargeUserCardUI extends AbstractUI {

    private final ChargeUserCardController theController = new ChargeUserCardController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        final String mecanographicNumber = Console.readLine("The user mecanographic number");
        final double amount = Console.readDouble("The amout");
        
        try {
            theController.addCredit(mecanographicNumber, amount);
        } catch (DataConcurrencyException ex) {
            Logger.getLogger(ChargeUserCardUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataIntegrityViolationException ex) {
            Logger.getLogger(ChargeUserCardUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public String headline() {
        return "Charge User Card";
    }
}
