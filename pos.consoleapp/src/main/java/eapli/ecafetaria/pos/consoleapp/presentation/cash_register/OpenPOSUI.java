/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafetaria.pos.consoleapp.presentation.cash_register;

import eapli.ecafeteria.application.cafeteria.OpenPOSController;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.PersistenceContext;
import eapli.framework.application.Controller;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.util.io.Console;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nuno Meireles <1150830@isep.ipp.pt>
 * @author José Guimarães <1150812@isep.ipp.pt>
 */
public class OpenPOSUI extends AbstractUI {

    private final OpenPOSController theController = new OpenPOSController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {
        
        MealTypeRepository mealTypeRepo = PersistenceContext.repositories().mealTypes();
        MealType type;
        Date data = new Date();
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(data);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        
        System.out.println("Open POS in this exact hour?");
        System.out.println("Day: " + day);
        System.out.println("Hour: " + hour);
        
        if(hour < 15) {
            System.out.println("Type of meal: Lunch");
        } else {
            System.out.println("Type of meal: Dinner");
        }
  
        System.out.println("1. Yes");
        System.out.println("2. Choose other time.");
        int option = Console.readOption(1, 2, 0);

        if (option == 1) {
            if(hour < 15) {
                type = mealTypeRepo.findByName(Designation.valueOf("lunch"));
            } else {
                type = mealTypeRepo.findByName(Designation.valueOf("dinner"));
            }
        } else {    
            System.out.println("Select day to work: (DD-MM-YYYY)");
            Scanner ler = new Scanner(System.in);
            String vec[] = ler.next().split("-");
            day = Integer.parseInt(vec[0]);
            int month = Integer.parseInt(vec[1]);
            int year = Integer.parseInt(vec[2]);
            calendar.set(year, month - 1, day);

            data = calendar.getTime();
            System.out.println("Select meal to work: ");
            System.out.println("1. Lunch");
            System.out.println("2. Dinner");

            option = Console.readOption(1, 2, 0);
            
            if (option == 1) {
                type = mealTypeRepo.findByName(Designation.valueOf("lunch"));
            } else {
                type = mealTypeRepo.findByName(Designation.valueOf("dinner"));
            }
        }
        
        try {
            theController.choosePOSToWork(data, type);
        } catch (DataConcurrencyException | DataIntegrityViolationException ex) {
            Logger.getLogger(OpenPOSUI.class.getName()).log(Level.SEVERE, null, ex);
        }
     
        return false;
    }

    @Override
    public String headline() {
        return "Open POS";
    }
}
