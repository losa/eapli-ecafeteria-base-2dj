/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.complaints;

import eapli.ecafeteria.domain.user.Booking;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author < José Miguel - 1150641@isep.ipp.pt >
 */
public class BookingPrinter implements Visitor<Booking> {

    @Override
    public void visit(Booking visitee) {
        System.out.printf("%-10s%-30s%-15s%-30s\n", visitee.user().id(), visitee.bookingDate().getTime(), visitee.meal().mealType(), visitee.meal().dishName());
    }

}
