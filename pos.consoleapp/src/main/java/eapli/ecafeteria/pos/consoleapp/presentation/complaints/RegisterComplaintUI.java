/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.complaints;

import eapli.ecafeteria.application.complaints.RegisterComplaintController;
import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.application.Controller;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.SelectWidget;
import eapli.util.io.Console;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gustavo (1140320), Vasco (1140338)
 */
public class RegisterComplaintUI extends AbstractUI {

    private final RegisterComplaintController theController = new RegisterComplaintController();

    protected Controller controller() {
        return this.theController;
    }

    @Override
    protected boolean doShow() {

        final String complaint = Console.readLine("Complaint");

        final SelectWidget<Dish> dishSelector = new SelectWidget<>("a mudar",theController.getDishes(), new DishPrinter());
        dishSelector.show();
        final Dish dish = dishSelector.selectedElement();
        if (dish == null){
            return false;
        }
        
        final String numeroMecanografico = Console.readLine("Mecanographic number");
        
        CafeteriaUser user = theController.getCafetariaUserByMecanographicNumber(numeroMecanografico);
        
        try {
            this.theController.registerComplaint(complaint, dish, user);
        } catch (DataIntegrityViolationException | DataConcurrencyException ex) {
            Logger.getLogger(RegisterComplaintUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }

    @Override
    public String headline() {
        return "Register Complaint";
    }
}
