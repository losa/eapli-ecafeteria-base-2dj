/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.complaints;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.framework.visitor.Visitor;

/**
 *
 * @author gustavo (1140320), Vasco (1140338)
 */
class DishPrinter implements Visitor<Dish> {

    @Override
    public void visit(Dish visitee) {
        System.out.printf("%-30s%-25s%-10s%-4s\n", visitee.name(), visitee.dishType().description(),
                visitee.currentPrice(), String.valueOf(visitee.isActive()));
    }
}

