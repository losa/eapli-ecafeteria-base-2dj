/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation.complaints;

import eapli.framework.actions.Action;

/**
 *
 * @author gustavo (1140320), Vasco (1140338)
 */
public class RegisterComplaintAction implements Action {

    @Override
    public boolean execute() {
        return new RegisterComplaintUI().show();
    }
}
