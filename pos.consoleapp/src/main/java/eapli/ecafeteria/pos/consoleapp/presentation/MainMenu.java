/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.pos.consoleapp.presentation;

import eapli.cafeteria.consoleapp.presentation.ExitWithMessageAction;
import eapli.cafeteria.consoleapp.presentation.MyUserMenu;
import eapli.ecafetaria.pos.consoleapp.presentation.cash_register.ChargeUserCardAction;
import eapli.ecafetaria.pos.consoleapp.presentation.cash_register.ClosePOSAction;
import eapli.ecafetaria.pos.consoleapp.presentation.cash_register.DeliveryMealAction;
import eapli.ecafetaria.pos.consoleapp.presentation.cash_register.OpenPOSAction;
import eapli.ecafeteria.AppSettings;
import eapli.ecafeteria.Application;
import eapli.ecafeteria.domain.authz.ActionRight;
import eapli.ecafeteria.domain.authz.UserSession;
import eapli.framework.actions.ReturnAction;
import eapli.ecafeteria.pos.consoleapp.presentation.complaints.RegisterComplaintAction;
import eapli.framework.presentation.console.AbstractUI;
import eapli.framework.presentation.console.HorizontalMenuRenderer;
import eapli.framework.presentation.console.Menu;
import eapli.framework.presentation.console.MenuItem;
import eapli.framework.presentation.console.MenuRenderer;
import eapli.framework.presentation.console.ShowVerticalSubMenuAction;
import eapli.framework.presentation.console.SubMenu;
import eapli.framework.presentation.console.VerticalMenuRenderer;
import eapli.framework.presentation.console.VerticalSeparator;

/**
 * TODO split this class in more specialized classes for each menu
 *
 * @author Paulo Gandra Sousa
 */
public class MainMenu extends AbstractUI {

    //SALES MENU
    private static final int OPEN_POS_OPTION = 1;
    private static final int DELIVERY_MEAL = 2;
    private static final int CLOSE_POS_OPTION = 3;
    private static final int EXIT_OPTION = 0;

    //USER MENU
    private static final int CHARGE_USER_CARD_OPTION = 1;
    private static final int REGISTER_COMPLAINT_OPTION = 2;

    // MAIN MENU
    private static final int MY_USER_OPTION = 1;
    private static final int CASHIER_MENU = 2;
    private static final int USERS_OPTION = 4;

    @Override
    public boolean show() {
        drawFormTitle();
        return doShow();
    }

    /**
     * @return true if the user selected the exit option
     */
    @Override
    public boolean doShow() {
        final Menu menu = buildMainMenu();
        final MenuRenderer renderer;
        if (Application.settings().isMenuLayoutHorizontal()) {
            renderer = new HorizontalMenuRenderer(menu);
        } else {
            renderer = new VerticalMenuRenderer(menu);
        }
        return renderer.show();
    }

    @Override
    public String headline() {
        return "eCafeteria POS [@" + Application.session().session().authenticatedUser().id() + "]";
    }

    private Menu buildMainMenu() {
        final Menu mainMenu = new Menu();

        final Menu myUserMenu = new MyUserMenu();
        mainMenu.add(new SubMenu(MY_USER_OPTION, myUserMenu, new ShowVerticalSubMenuAction(myUserMenu)));

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.add(VerticalSeparator.separator());
        }

        if (Application.session().session().authenticatedUser().isAuthorizedTo(ActionRight.SALE)) {
            final Menu cashierMenu = buildCashierMenu();
            mainMenu.add(new SubMenu(CASHIER_MENU, cashierMenu, new ShowVerticalSubMenuAction(cashierMenu)));
            final Menu usersMenu = buildUsersMenu();
            mainMenu.add(new SubMenu(USERS_OPTION, usersMenu, new ShowVerticalSubMenuAction(usersMenu)));
        }

        if (!Application.settings().isMenuLayoutHorizontal()) {
            mainMenu.add(VerticalSeparator.separator());
        }

        mainMenu.add(new MenuItem(EXIT_OPTION, "Exit", new ExitWithMessageAction()));

        return mainMenu;
    }

    private Menu buildCashierMenu() {

        final Menu menu = new Menu("Cashier Menu >");
        if (UserSession.getPOS() == null || !UserSession.getPOS().POSActive()) {
            menu.add(new MenuItem(OPEN_POS_OPTION, "Open POS", new OpenPOSAction()));
        } else {
            // To do
            if (UserSession.getPOS() != null && UserSession.getPOS().POSActive()) {
                menu.add(new MenuItem(DELIVERY_MEAL, "Delivery Meal", new DeliveryMealAction()));
            }
            if (UserSession.getPOS() != null && UserSession.getPOS().POSActive()) {
                menu.add(new MenuItem(CLOSE_POS_OPTION, "Close POS", new ClosePOSAction()));
            }
        }
//        if (UserSession.getPOS() != null && UserSession.getPOS().POSActive()) {
//            menu.add(new MenuItem(DELIVERY_MEAL, "Delivery Meal", new DeliveryMealAction()));
//        }
//        if (UserSession.getPOS() != null && UserSession.getPOS().POSActive()) {
//            menu.add(new MenuItem(CLOSE_POS_OPTION, "Close POS", new ClosePOSAction()));
//        }
        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;

    }

    private Menu buildUsersMenu() {
        final Menu menu = new Menu("Users >");

        menu.add(new MenuItem(CHARGE_USER_CARD_OPTION, "Charge User Card",
                new ChargeUserCardAction()));

        menu.add(new MenuItem(REGISTER_COMPLAINT_OPTION, "Register Complaint",
                new RegisterComplaintAction()));

        menu.add(new MenuItem(EXIT_OPTION, "Return ", new ReturnAction()));

        return menu;
    }
}
