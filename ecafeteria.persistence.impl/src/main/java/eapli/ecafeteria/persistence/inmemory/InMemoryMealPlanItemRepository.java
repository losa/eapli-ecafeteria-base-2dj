/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author jferr
 */
public class InMemoryMealPlanItemRepository extends InMemoryRepositoryWithLongPK<MealPlanItem> implements MealPlanItemRepository {

    @Override
    public Iterable<MealPlanItem> findMealPlanItemByDateAndType(Calendar date, MealType type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<MealPlanItem> findMealPlanItemsByMealPlan(MealPlan mealPlan) {
        List<MealPlanItem> mealPlanItemList = new ArrayList<>();
        for (MealPlanItem mealPlanItem : this.findAll()) {
            if (mealPlanItem.mealPlan().equals(mealPlan)) {
                mealPlanItemList.add(mealPlanItem);
            }
        }
        return mealPlanItemList;
    }
    
}
