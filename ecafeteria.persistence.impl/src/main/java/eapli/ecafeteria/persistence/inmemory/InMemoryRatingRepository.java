/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.user.Rating;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.RatingRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;

/**
 *
 * @author Miguel Minnemann (1150457) & Daniel Ribeiro (1150577)
 */
public class InMemoryRatingRepository extends InMemoryRepositoryWithLongPK<Rating> implements RatingRepository{

    @Override
    public Iterable<Rating> findAllRatingsByUser(SystemUser user) {
        return match(e -> e.user().equals(user));
    }

    @Override
    public Iterable<Rating> findAllRatingsByMeal(Meal meal) {
        //TODO to be implemented when meal is ready
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Rating> findAllRatingsByDish(Dish dish) {
        return match(e -> e.meal().dishFromMeal().equals(dish));
    }
}
