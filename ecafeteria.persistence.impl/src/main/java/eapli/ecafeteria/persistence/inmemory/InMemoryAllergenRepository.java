/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepository;

/**
 *
 * @author luis
 */
public class InMemoryAllergenRepository extends InMemoryRepository<Allergen, String> implements AllergenRepository {

    @Override
    public Allergen findByName(String name) {
        return matchOne(e -> e.name().equals(name));
    }

    @Override
    protected String newPK(Allergen entity) {
        return entity.id();
    }
    }
