/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.POSRepository;
import eapli.framework.persistence.DataIntegrityViolationException;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;
import java.util.Date;
import java.util.Iterator;
import java.util.Optional;

/**
 *
 * @author Nuno Meireles <1150830@isep.ipp.pt>
 * @author José Guimarães <1150812@isep.ipp.pt>
 */
class InMemoryPOSRepository extends InMemoryRepositoryWithLongPK<POS> implements POSRepository {
    
    public InMemoryPOSRepository() {
    }

    @Override
    public POS findByDate(Date date) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
