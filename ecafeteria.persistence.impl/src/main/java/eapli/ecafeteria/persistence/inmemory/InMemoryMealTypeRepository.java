package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;

public class InMemoryMealTypeRepository extends InMemoryRepositoryWithLongPK<MealType> implements MealTypeRepository   {

    @Override
    public MealType findByName(Designation name) {
        return matchOne(e -> e.name().equals(name));
    }
    
}
