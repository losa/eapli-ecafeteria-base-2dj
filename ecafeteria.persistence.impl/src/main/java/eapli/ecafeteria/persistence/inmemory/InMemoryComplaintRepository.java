/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.complaints.Complaint;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.persistence.ComplaintRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;

/**
 *
 * @author gustavo (1140320), Vasco (1140338)
 */
public class InMemoryComplaintRepository extends InMemoryRepositoryWithLongPK<Complaint> implements ComplaintRepository{

    @Override
    public Iterable<Complaint> findAllComplaintsByUser(SystemUser user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Complaint> findAllComplaintsByDish(Dish dish) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
