package eapli.ecafeteria.persistence.inmemory;

//import eapli.ecafeteria.bootstrapers.ECafeteriaBootstraper;
import eapli.ecafeteria.bootstrapers.ECafeteriaBootstraper;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.ecafeteria.persistence.BookingRepository;
import eapli.ecafeteria.persistence.CafeteriaUserRepository;
import eapli.ecafeteria.persistence.DishAllergenRepository;
import eapli.ecafeteria.persistence.ComplaintRepository;
import eapli.ecafeteria.persistence.DishRepository;
import eapli.ecafeteria.persistence.DishTypeRepository;
import eapli.ecafeteria.persistence.MaterialRepository;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.ecafeteria.persistence.MealRepository;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.ecafeteria.persistence.OrganicUnitRepository;
import eapli.ecafeteria.persistence.POSRepository;
import eapli.ecafeteria.persistence.RatingRepository;
import eapli.ecafeteria.persistence.RepositoryFactory;
import eapli.ecafeteria.persistence.SignupRequestRepository;
import eapli.ecafeteria.persistence.UserAllergenRepository;
import eapli.ecafeteria.persistence.UserRepository;
import eapli.ecafeteria.persistence.jpa.JpaCafeteriaUserRepository;
import eapli.ecafeteria.persistence.jpa.JpaDishAllergenRepository;
import eapli.framework.persistence.repositories.TransactionalContext;

/**
 *
 * Created by nuno on 20/03/16.
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    static {
        // only needed because of the in memory persistence
        new ECafeteriaBootstraper().execute();
    }

    @Override
    public DishTypeRepository dishTypes() {
        return new InMemoryDishTypeRepository();
    }

    @Override
    public OrganicUnitRepository organicUnits() {
        return new InMemoryOrganicUnitRepository();
    }

    @Override
    public CafeteriaUserRepository cafeteriaUsers(TransactionalContext autoTx) {
        return null; // FIXME para compilar
    }

    @Override
    public SignupRequestRepository signupRequests(TransactionalContext autoTx) {
        return null; // FIXME para compilar
    }

    @Override
    public DishRepository dishes() {
        return new InMemoryDishRepository();
    }

    @Override
    public MaterialRepository materials() {
        return new InMemoryMaterialRepository();
    }

    @Override
    public RatingRepository ratings() {
        return new InMemoryRatingRepository();
    }

    @Override
    public AllergenRepository Allergens() {
        return new InMemoryAllergenRepository();
    }

    @Override
    public BookingRepository bookings() {
        return new InMemoryBookingRepository();
    }

    @Override
    public MenuRepository menus() {
        return new InMemoryMenuRepository();
    }

    @Override
    public MealTypeRepository mealTypes() {
        return new InMemoryMealTypeRepository();
    }

    @Override
    public MealPlanRepository mealPlans() {
        return new InMemoryMealPlanRepository();
    }

    @Override
    public MealRepository meals() {
        return new InMemoryMealRepository();
    }

    @Override
    public TransactionalContext buildTransactionalContext() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UserRepository users(TransactionalContext autoTx) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public POSRepository pos() {
        return new InMemoryPOSRepository();
    }    

    @Override
    public DishAllergenRepository dishAllergens() {
        return new InMemoryDishAllergenRepository();
    }

    @Override
    public ComplaintRepository complaints() {
        return new InMemoryComplaintRepository();
    }

    @Override
    public MealPlanItemRepository mealPlanItems() {
        return new InMemoryMealPlanItemRepository();

    }

    @Override
    public UserAllergenRepository userAllergens() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
