package eapli.ecafeteria.persistence.inmemory;

import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MenuRepository;
import eapli.framework.persistence.repositories.impl.inmemory.InMemoryRepositoryWithLongPK;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class InMemoryMenuRepository extends InMemoryRepositoryWithLongPK<Menu> implements MenuRepository  {

    @Override
    public Iterable<Menu> publishedMenus() {
        List<Menu> publishedMenu = new ArrayList();
        
        for (Menu menu : this.findAll()) {
            
            if(menu.isPublished()) {
                publishedMenu.add(menu);
            }
            
        }
        
        return publishedMenu;
    }

    @Override
    public Iterable<Menu> unpublishedMenus() {
        List<Menu> unpublishedMenu = new ArrayList();
        
        for (Menu menu : this.findAll()) {
            
            if(!menu.isPublished()) {
                unpublishedMenu.add(menu);
            }
            
        }
        
        return unpublishedMenu;
    }

    @Override
    public Iterable<Menu> thisWeekMenus() {
        List<Menu> thisWeekMenus = new ArrayList();
        
        Calendar beginningCurrentWeek = DateTime.beginningOfWeek(DateTime.currentYear(), DateTime.currentWeekNumber());
        
        Calendar endCurrentWeek = DateTime.endOfWeek(DateTime.currentYear(), DateTime.currentWeekNumber());
        
        for (Menu menu : this.findAll()) {
            
            if(menu.isInPeriod(beginningCurrentWeek, endCurrentWeek) && menu.isPublished()) {
                thisWeekMenus.add(menu);
            }
            
        }
        
        return thisWeekMenus;
    }
    
    
    
}
