/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.MealPlanItem;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealPlanItemRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author jferr
 */
public class JpaMealPlanItemRepository extends CafeteriaJpaRepositoryBase<MealPlanItem, Long> implements MealPlanItemRepository {

    @Override
    public Iterable<MealPlanItem> findMealPlanItemByDateAndType(Calendar date, MealType type) {
        Query createQuery = entityManager().createQuery("SELECT e FROM MealPlanItem e WHERE e.date=:date AND e.meal.mealType=:type");
        createQuery.setParameter("date", date);
        createQuery.setParameter("type", type);
        return createQuery.getResultList();
    }

    @Override
    public Iterable<MealPlanItem> findMealPlanItemsByMealPlan(MealPlan mealPlan) {
        List<MealPlanItem> mealPlanItemList = new ArrayList<>();
        for (MealPlanItem mealPlanItem : this.findAll()) {
            if (mealPlanItem.mealPlan().equals(mealPlan)) {
                mealPlanItemList.add(mealPlanItem);
            }
        }
        return mealPlanItemList;
    }
    
}
