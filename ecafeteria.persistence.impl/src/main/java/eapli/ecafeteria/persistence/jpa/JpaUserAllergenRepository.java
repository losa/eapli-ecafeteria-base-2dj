/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.UserAllergen;
import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.UserAllergenRepository;
import javax.persistence.Query;

/**
 *
 * @author Rui Peixoto
 */
public class JpaUserAllergenRepository extends CafeteriaJpaRepositoryBase<UserAllergen, Long> implements UserAllergenRepository {

    @Override
    public Iterable<UserAllergen> findAllergenByCafeteriaUser(CafeteriaUser cafeteriaUser) {
        Query createQuery = entityManager().createQuery("SELECT e FROM UserAllergen e WHERE e.cafeteriaUser=:user");
        createQuery.setParameter("user", cafeteriaUser);
        return createQuery.getResultList();
    }

    public UserAllergen checkIfAllergenIsRegisted(CafeteriaUser cafeteriaUser, Allergen allergen) {
        return matchOne("e.cafeteriaUser=:user AND e.allergen=:allerg", "user", cafeteriaUser, "allerg", allergen);
    }

}
