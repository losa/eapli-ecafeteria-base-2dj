/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.complaints.Complaint;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.persistence.ComplaintRepository;

/**
 *
 * @author gustavo (1140320)
 */
public class JpaComplaintRepository extends CafeteriaJpaRepositoryBase<Complaint, Long> implements ComplaintRepository{

    @Override
    public Iterable<Complaint> findAllComplaintsByUser(SystemUser user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Complaint> findAllComplaintsByDish(Dish dish) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
