/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.meals.Menu;
import eapli.ecafeteria.persistence.MealRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Query;

/**
 *
 * @author jferr
 */
public class JpaMealRepository extends CafeteriaJpaRepositoryBase<Meal, Long> implements MealRepository {

    @Override
    public Iterable<Meal> findByPeriod(Calendar startingDate, Calendar endDate) {
        List<Meal> mealsInPeriod = new ArrayList();

        for (Meal meal : this.findAll()) {

            if (meal.dateIsInPeriod(startingDate, endDate)) {
                mealsInPeriod.add(meal);
            }

        }

        return mealsInPeriod;
    }

    @Override
    public Iterable<Meal> findPublishedMealsInPeriod(Calendar startingDate, Calendar endDate) {
        List<Meal> meals = new ArrayList();

        for (Meal meal : this.findAll()) {

            if (meal.dateIsInPeriod(startingDate, endDate) && meal.menu().isPublished()) {
                meals.add(meal);
            }

        }

        return meals;
    }

    @Override
    public Iterable<Meal> findByMenu(Menu menu) {
        List<Meal> mealsWithMenu = new ArrayList();

        for (Meal meal : this.findAll()) {

            if (meal.menu().equals(menu)) {
                mealsWithMenu.add(meal);
            }

        }

        return mealsWithMenu;
    }

    @Override
    public Iterable<Meal> findByDate(Calendar date) {
        Query createQuery = entityManager().createQuery("SELECT e FROM Meal e WHERE e.ofDay=:date");
        createQuery.setParameter("date", date);
        return createQuery.getResultList();
    }

    @Override
    public Iterable<Meal> findByDish(Dish dish) {
        List<Meal> mealsByDish = new ArrayList();

        for (Meal meal : this.findAll()) {

            if (meal.dishName().equals(dish.name())) {
                mealsByDish.add(meal);
            }

        }

        return mealsByDish;
    }

    @Override
    public Iterable<Meal> findByMealType(MealType mealType) {
        List<Meal> mealsByMealType = new ArrayList();

        for (Meal meal : this.findAll()) {

            if (meal.mealType().equals(mealType.name())) {
                mealsByMealType.add(meal);
            }

        }

        return mealsByMealType;
    }
}
