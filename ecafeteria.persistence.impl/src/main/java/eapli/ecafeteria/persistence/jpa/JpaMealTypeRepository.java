package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.persistence.MealTypeRepository;
import eapli.framework.domain.Designation;

public class JpaMealTypeRepository extends CafeteriaJpaRepositoryBase<MealType, Long> implements MealTypeRepository  {

    @Override
    public MealType findByName(Designation name) {
        return matchOne("e.name.theDesignation='" + name + "'");
    }
    
}
