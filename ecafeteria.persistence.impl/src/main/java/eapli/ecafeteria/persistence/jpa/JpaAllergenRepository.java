/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.Allergen;
import eapli.ecafeteria.persistence.AllergenRepository;
import eapli.framework.domain.Designation;
import eapli.framework.persistence.DataConcurrencyException;
import eapli.framework.persistence.DataIntegrityViolationException;
import java.util.Iterator;
import java.util.Optional;

/**
 *
 * @author luis
 */
public class JpaAllergenRepository extends CafeteriaJpaRepositoryBase<Allergen, String> implements AllergenRepository {

    @Override
    public Allergen findByName(String name) {
        // TODO use parameters instead of string concatenation
        return matchOne("e.name=:name", "name", name);
    }

}
