/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.meals.MealPlan;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.MealPlanRepository;
import eapli.util.DateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author jferr
 */
public class JpaMealPlanRepository extends CafeteriaJpaRepositoryBase<MealPlan, Long> implements MealPlanRepository {

    @Override
    public List<Meal> findMealPlanByDate(Calendar date) {
        List<Meal> list = new ArrayList<>();
        for (MealPlan mp : this.findAll()) {
            //TODO implement to adapt to MealPlan and MealPlanItem changes
        }
        return list;
    }

    @Override
    public Iterable<MealPlan> findValidMealPlans() {
        List<MealPlan> mealPlanList = new ArrayList<>();
        Calendar today = DateTime.now();
        for (MealPlan mealPlan : this.findAll()) {
            if (mealPlan.endDate().after(today)) {
                mealPlanList.add(mealPlan);
            }
        }
        return mealPlanList;
    }
    
    
}
