package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.kitchen.Lot;
import eapli.ecafeteria.persistence.LotRepository;


class JpaLotRepository extends CafeteriaJpaRepositoryBase<Lot, Long>
                        implements LotRepository{
    
    
    @Override
    public Lot findByAcronym(String lotCode) {
        return matchOne("e.id=:lotCode", "lotCode", lotCode);
    }
    
    
}
