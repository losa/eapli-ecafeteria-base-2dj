/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.cafeteria.CafeteriaUser;
import eapli.ecafeteria.domain.cafeteria.MecanographicNumber;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.DishType;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.domain.meals.MealType;
import eapli.ecafeteria.domain.user.Booking;
import eapli.ecafeteria.persistence.BookingRepository;
import java.util.Calendar;
import javax.persistence.Query;

/**
 *
 * @author Miguel Minnemann (1150457)
 */
public class JpaBookingRepository extends CafeteriaJpaRepositoryBase<Booking, Long> implements BookingRepository {

    @Override
    public Iterable<Booking> findAllBookingsByUser(CafeteriaUser user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Iterable<Booking> findAllBookingsByMeal(Meal meal) {
        Query createQuery = entityManager().createQuery("SELECT e FROM Booking e WHERE e.meal=:meal");
        createQuery.setParameter("meal", meal);
        return createQuery.getResultList();
    }

    @Override
    public Iterable<Booking> allBookingsDeliveredToUser(CafeteriaUser user) {
        Query createQuery = entityManager().createQuery("SELECT e FROM Booking e WHERE e.user=:user");
        createQuery.setParameter("user", user);
        return createQuery.getResultList();
    }

    @Override
    public Iterable<Booking> findAllBookingsByDish(Dish dish) {
        Query createQuery = entityManager().createQuery("SELECT e FROM Booking e WHERE e.meal.dish=:dish");
        createQuery.setParameter("dish", dish);
        return createQuery.getResultList();
    }

    @Override
    public Iterable<Booking> findAllBookingsByDishType(DishType dishType) {
        Query createQuery = entityManager().createQuery("SELECT e FROM Booking e WHERE e.meal.dish.dishType=:dishType");
        createQuery.setParameter("dishType", dishType);
        return createQuery.getResultList();
    }

    @Override
    public Iterable<Booking> findAllBookingsByDay(Calendar date) {
        Query createQuery = entityManager().createQuery("SELECT e FROM Booking e WHERE e.date=:date");
        createQuery.setParameter("date", date);
        return createQuery.getResultList();
    }

    @Override
    public Iterable<Booking> findAllBookingsByDayAndMealType(Calendar date, MealType mealType) {
        Query createQuery = entityManager().createQuery("SELECT e FROM Booking e WHERE e.meal.ofDay=:date AND e.meal.mealType=:mealType AND e.state LIKE '0'");
        createQuery.setParameter("date", date);
        createQuery.setParameter("mealType", mealType);
        return createQuery.getResultList();
    }

}
