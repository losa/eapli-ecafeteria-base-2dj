/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.user.Rating;
import eapli.ecafeteria.domain.authz.SystemUser;
import eapli.ecafeteria.domain.meals.Dish;
import eapli.ecafeteria.domain.meals.Meal;
import eapli.ecafeteria.persistence.RatingRepository;
import javax.persistence.Query;

/**
 *
 * @author Miguel Minnemann (1150457) & Daniel Ribeiro (1150577)
 */
public class JpaRatingRepository extends CafeteriaJpaRepositoryBase<Rating, Long> implements RatingRepository {

    @Override
    public Iterable<Rating> findAllRatingsByUser(SystemUser user) {
        //TODO is this the right implementation?
        return match("e.user.username.name='" + user.username() + "'");
    }

    @Override
    public Iterable<Rating> findAllRatingsByMeal(Meal meal) {
        //TODO implement the method when meal is ready
        return match("e.meal.pk=" + meal.id());

    }

    @Override
    public Iterable<Rating> findAllRatingsByDish(Dish dish) {
        Query createQuery = entityManager().createQuery("SELECT e FROM Rating e WHERE e.meal.dish=:dish");
        createQuery.setParameter("dish", dish);
        return createQuery.getResultList();
    }

}
