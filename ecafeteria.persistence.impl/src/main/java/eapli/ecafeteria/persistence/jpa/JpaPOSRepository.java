/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.ecafeteria.persistence.jpa;

import eapli.ecafeteria.domain.cafeteria.POS;
import eapli.ecafeteria.persistence.POSRepository;
import java.util.Date;

/**
 *
 * @author Nuno Meireles <1150830@isep.ipp.pt>
 * @author José Guimarães <1150812@isep.ipp.pt>
 */
class JpaPOSRepository extends CafeteriaJpaRepositoryBase<POS, Long> implements POSRepository {
    
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    @Override
    public POS findByDate(Date date) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
